---
title: Boucle vers la forêt par les pistes
date: 2025-01-05T09:00:00.000Z
author: Pierrick Hospital
tags:
  - balade
  - forêt
  - forêt de Bastard
  - Garcia Lorca
  - Paris-Madrid
  - Peyroulet
  - Léon Jouhaux
fmContentType: balade
---

Ce dimanche, nous sommes partis faire un petit tour jusqu’à la forêt de Bastard (aussi appelée bois de Pau). Nous avons profité de ce cadre pour partager quelques petits gâteaux.

{{< openrunner id="20466778" img="parcours.jpg" >}}

Le trajet à l’aller traversait la ville du sud au nord en passant par la piste cyclable de l’avenue Garcia Lorca, et la toute nouvelle piste de l'avenue Léon Jouhaux. Pour le retour, nous avons emprunté un bout du Cami Salié, puis rejoint la Paris-Madrid. Nous sommes finalement rentrés par l’avenue Béziou, et la place Verdun.


Nous étions de retour vers midi et demi, après avoir parcouru environ 17km.

{{< gallery photos >}}
