---
title: À l'est jusqu'à Lée
date: 2025-02-02T09:00:00.000Z
author: Pierrick Hospital
tags:
    - Boulevard des Pyrénées
    - gare
    - Hédas
    - Lée
    - Ousse
    - Idron
fmContentType: balade
---
Ce dimanche matin, nous étions 20 cyclistes prêt à braver la froidure pour ce petit tour vers l’Est.

La vue depuis le boulevard des Pyrénées était un peu bouchée à 10h.
Mais très vite, les brumes matinales se sont évaporées, et nous avons pu profiter d’un magnifique soleil.
Durant tout le trajet à l’aller, nous avons pu profiter de la vue sur les Pyrénées enneigées : c’était superbe !

Arrivés à Lée, nous nous sommes arrêtés pour discuter et grignoter des petits gâteaux (merci Alexis) et des crêpes (merci Odile).
Le soleil nous réchauffait, c’était un moment très agréable.

{{< gallery photos >}}

Nous sommes ensuite repartis en restant dans la vallée du ruisseau de l’Ousse.
Lors d’une bifurcation à gauche, quelques cyclistes qui discutaient n’ont pas vu le groupe et sont partis tout droit.
Heureusement que Alexis est allé les chercher pour qu’ils rejoignent le groupe.

Arrivés à la gare à Pau, nous comptions passer par les sentiers du Roy pour remonter sur le boulevard, mais le chemin en haut du funiculaire était fermé aux piétons pour travaux.
Nous avons choisi de passer par la place de la Monnaie et le Hédas, mais ce n’était pas une très bonne idée, car il aurait fallu pousser les vélos dans les escaliers de la place d’Espagne.  
Nous sommes donc remontés depuis la place Récaborde vers la rue Tran avant de nous disperser.

{{< openrunner id="20656294" >}}

C’était une chouette balade, inédite pour beaucoup des participants.
À la prochaine!

