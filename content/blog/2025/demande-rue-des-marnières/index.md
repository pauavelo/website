---
draft: true
title: Demande rue des marnières
date: 2025-01-05T13:39:41.125Z
author: ""
tags:
  - Billère
  - Marnières
  - courrier
header: courrier.png
---
Jocelyn Mounié, un membre de notre groupe aménagement, a fait parvenir une demande à la ville de Billère au sujet de l'aménagement du haut de la piste cyclable de la rue des Marnières. Nous vous retranscrivons son contenu ici.

> Bonjour,
>
> Je me permets de vous contacter concernant la rue des Marnières en tant qu'usager circulant à vélo dans le cadre de mon trajet domicile-travail.
>
> Depuis quelques mois, le sas vélo au feu à la limite avec la ville de Pau a disparu, pouvez-vous demander à vos équipes de le repeindre ?
>
> Egalement, la sortie de la piste cyclable n'est pas sécurisée :
>
> ![](20240502-182658.jpg)
>
> - Il peut y avoir une différence de vitesse importante entre les usagers, les vélos sont lents du fait de la montée.
> - Les véhicules peuvent être arrêtés devant la fin de la piste cyclable en attendant le feu vert. Les vélos doivent donc slalomer pour remonter, avec tous les risques que cela comporte.
>
> Est-il possible de prolonger cette piste jusqu'au sas vélo ?
> (en réduisant la largeur des voies voiture ou en supprimant une voie, par la mise en place d'un aménagement type "coronapiste" avant un aménagement pérenne, ...) 
>
> Je vous remercie par avance de l'attention portée à ma demande,
>
> Bonne journée,

