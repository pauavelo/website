---
title: Consulation Rives du Gave
date: 2025-01-30T20:06:00.000Z
author: Antoine Lefeuvre
tags:
  - aménagements
  - analyse
  - proposition
  - rives du gave
  - véloroute V81
  - REV
---

Vous retrouverez dans ce dossier l’ensemble des observations de l’association Pau à vélo concernant [le projet Rives de Gave](https://www.pau.fr/communes-institutions-realisations/les-grands-projets/rives-du-gave-un-projet-urbain-novateur), dans le cadre de [la concertation publique à son sujet](https://www.pau.fr/actualites/concertation-publique-projet-de-zac-rives-du-gave).

<div class="pure-g trombi">
 {{< dossier id="rives-gaves" title="Contribution Pau à Vélo au projet Rives du Gave" >}}
</div>

L’association se réjouit de voir un projet d’aménagement urbain basé sur des principes de santé publique et environnementaux forts. Nous espérons que ce nouveau quartier Rives de Gave sera emblématique de ce qu’il est possible de faire en terme de mobilités au sein de l’agglomération paloise : réduire la place de la voiture, y compris la place du stationnement dans l’espace public, favoriser la marche et le vélo pour la plupart des déplacements et proposer des transports en commun efficaces.

Nous jugeons néanmoins incompatible le développement d’un tel quartier avec le mantien du Grand Prix automobile. Au-delà du fait que les principes du Grand Prix sont antagonistes à ceux de Rives de Gaves, le circuit automobile qu’est aujourd’hui l’avenue Gaston Lacoste devra être transformé en un boulevard urbain pacifié pour être un lien, et non une frontière, entre le nouveau quartier et le centre-ville.

Ce dossier comportes des propositions faites par Pau à Vélo pour tirer partie de la position privilégiée de Rives en Gave afin d’en faire un véritable hub du réseau express vélo (chapitre 2), pour mieux connecter le nouveau quartier au centre-ville (chapitre 3) ainsi qu’au quartier du Buisson, où se trouvera l’école du secteur, et à Bizanos (chapitre 4). L’association Pau à Vélo se tient à la disposition des collectivités et des aménageurs pour discuter et approfondir ces propositions.