---
title: Vélorution et ciné
date: 2025-01-31T22:38:00.000Z
author: "Sébastien Lamy"
tags:
  - animations
  - ciné
  - vélorution
---

Ce vendredi 31 janvier, le groupe "animations" de Pau à Vélo organisait la projection du film [un monde sous vide](https://www.swissfilms.ch/fr/movie/un-monde-sous-vide/CCD771D4075045638BBEE14C71004CF8) au Méliès, à Pau, en présence du réalisateur Fabien Favre. À cette occasion, de nombreux spectateurs se sont rejoint pour une vélorution avant la projection.

Le programme de la soirée du vendredi 31 janvier a donc été :
* 18h : départ de la vélorution place de la reine Marguerite à Pau
* 19h : Un petit temps pour manger, il était possible de réserver une restauration au bar du Méliès
* 20h : Projection du film "Un monde sous vide" Au Méliès.

{{< gallery photos >}}

À mi-chemin entre le documentaire environnemental et le film de voyage, le film "Un monde sous vide retrace l'histoire d'une incroyable épopée à travers l'Europe. De Tarifa au Cap Nord, Fabien a parcouru 8 000 kilomètres à vélo en tentant de s'affranchir du moindre emballage en plastique. Et donner la paroles à ces personnes qui, d'un bout à l'autre du continent, cherchent à libérer la planète de cette matière qui l'étouffe.

À travers cette singulière expérience, et en rapprochant les messages des scientifiques à ceux des citoyens, Fabien nous invite à interroger nos modes de consommation, et plus globalement notre rapport au monde.

Le film a été suivi d'un débat enrichissant et qui a ouvert des perspectives d'action contre la pollution plastique pour certains, de voyage à vélo pour d'autres, les questions du public était nombreuses.