---
title: AG 2024
date: 2025-02-19T22:00:00.000Z
author: Sébastien Lamy
tags:
  - AG
---

Ce 19 février 2025, c'était notre assemblée Générale. L'occasion de faire le bilan de toutes les actions réalisées en 2024, et d'envisager les perspectives pour 2025. Vous pouvez consulter le diaporama qui a été projeté à cette occasion. Beaucoup beaucoup de travail ! Et beaucoup à faire encore en 2025 !

<div class="pure-g trombi">
 {{< dossier id="ag-2024" title="Diaporama de l'AG" >}}
</div>

À L'ordre du jour il y avait :
* Rapport d’activités 2024
* Bilan du conseil d’administration et des groupes de travail
* Rapport financier 2024
* Renouvellement du Conseil d'Administration
* Perspectives pour 2025 et fonctionnement par groupes/projets
* Questions diverses


En fin de réunion, un participant a évoqué un fort mécontentement au sujet de la position de notre association sur le carrefour « à la hollandaise » entre les avenues château d'Este et Lalanne, à Billère. Il estimait que la priorité qui y est donnée au vélo et qui est soutenue par l'association pose des problèmes de sécurité inacceptables. Notre association défend en effet un changement des mentalités, et sur les axes cyclables importants, une continuité aux intersections associée à un régime de priorité favorable au vélo. Ce régime peut être comparé à celui des piétons sur les passages piétons : Il définit une volonté politique, mais suppose également un réalisme et une vigilance pour le cycliste. Un autre participant à fait part du fait qu'il avait observé que ce régime de priorité avait entrainé des changements de comportements des automobilistes très positifs, sur l'entrée Est de Pau (carrefour Batsalle) 

L'évènement s'est terminé dans la bonne humeur, autour d'un pot de l'amitié très convivial.

{{< gallery photos >}}

