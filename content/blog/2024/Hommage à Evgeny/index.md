---
title: "Hommage à Evgeny"
date: 2024-01-25T12:15:21+01:00
author: "Sébastien Lamy"
tags:
  - hommage
  - rond-point
  - rond-point Gabriel Delaunay
  - médias
  - sécurité
  - accident
---

L'Association "Pau à Vélo" a déposé un vélo blanc à la mémoire d'Evgeny Bessonov, cycliste vélotaffeur [victime d'un accident mortel le 15 septembre 2023 au giratoire Gabriel Delaunay](https://www.sudouest.fr/societe/securite-routiere/pau-un-cycliste-meurt-dans-un-accident-avec-un-camion-16653774.php). Sa femme Diana, de nombreux collègues, ainsi que des cyclistes du quotidien étaient présents à cet hommage.

Christophe Nussbaumer, collège d'Evgeny et administrateur de Pau à Vélo, a pris la parole pour exprimer notre incompréhension, notre douleur, et nos espoirs pour l'avenir.

> Chers amis, chers collègues, chers cyclistes occasionnels ou du quotidien, chers tous,
>
> C'est ici, au bord du giratoire Gabriel Delaunay, que Evgeny a été mortellement accidenté le 15 septembre 2023. Ce tragique événement, nombre d'entre nous s'en souviennent très bien, c'était à l'heure d'arrivée au travail et la journée s'annonçait belle. Mais le destin en a voulu autrement.
>
>Evgeny était pourtant un cycliste aguerri, familier de cet itinéraire, il descendait l'avenue Alfred Nobel pour rejoindre l'entrée Ramadier du CSTJF, il avait l'habitude de négocier ce giratoire. Mais voilà, ce matin-là vers 9 heures, ce camion l'a brutalement percuté.
>
>Très apprécié par ses amis et par ses collègues chez TotalEnergies, Evgeny est parti beaucoup trop tôt à l'âge de 38 ans, laissant dans une profonde tristesse son épouse Diana et leurs enfants Aleksandra (5 ans) et Michael (2 ans). C'est trop injuste, si jeune, si plein de vie, pourquoi devait-il tomber là, ce jour-là ? Cette incompréhension nous la partageons avec sa famille. Cette tragédie laisse un vide incommensurable.
>
>Evgeny restera dans nos souvenirs comme un symbole, celui de l'insécurité routière qui menace tous les usagers vulnérables. Ce n'est pas une fatalité, cette situation doit pouvoir être améliorée pour sauver des vies. Il reste tant à faire pour que les routes soient suffisamment sécurisées pour les cyclistes et aussi les piétons.
>
>On sait que les giratoires génèrent souvent du danger, mais il doit être possible de mettre en place des aménagements apportant une meilleure protection, et une modération du trafic motorisé permettant de réduire le risque. Nous enjoignons les collectivités en charge du réseau routier à mettre en place urgemment des solutions efficaces, pour qu'un tel drame inacceptable ne se reproduise pas.
>
>Nous enjoignons aussi chacun à repenser sa façon de se déplacer au quotidien, à mesurer sa responsabilité sur cet espace commun qu'est la voirie. Le choix d'Evgeny de se déplacer à vélo, moyen de transport sobre et silencieux, bon pour la santé et pour la qualité de notre air, reste un modèle à suivre chaque fois que c'est possible pour les déplacements du quotidien. Sur ce giratoire, Evgeny était un pionnier courageux évoluant en milieu hostile. Nous appelons de nos vœux une voirie pacifiée et sécurisée, sur laquelle pourrait être plus largement suivi le choix qu'avait fait Evgeny.
>
>Chers tous, un grand merci de vous être joints à cette cérémonie, un grand merci pour apporter ainsi un peu de réconfort à Diana et ses enfants.
>
>Nous allons maintenant procéder à l'installation du vélo blanc à la mémoire d'Evgeny. Ce vélo blanc préparé par l'association "Pau à Vélo" vient s'ajouter aux autres, déjà nombreux, qui ont été déposés en plusieurs endroits de notre agglomération. Ils rappellent les risques que le trafic motorisé fait courir aux usagers vulnérables, ils sont comme un message à tous les conducteurs : ralentissez, regardez, déplacez-vous autrement, la vie est si précieuse !
>
Merci encore pour votre présence.

Comme Evgeny était d'origine étrangère, le discours a également été prononcé en anglais.

>
>Dear friends, dear colleagues, dear occasional or everyday cyclists, dear all,
>
>It is here, at the edge of the Gabriel Delaunay roundabout, that Evgeny was fatally injured on September 15, 2023. This tragic event, many of us remember it very well, it was at the time of arrival at the work and the day looked good. But fate wanted it otherwise.
>
>Evgeny was however a seasoned cyclist, familiar with this route, he went down Avenue Alfred Nobel to reach the Ramadier entrance to the CSTJF, he was used to negotiating this roundabout. But that morning, around 9 a.m., this truck suddenly hit him.
>
>Much appreciated by his friends and colleagues at TotalEnergies, Evgeny left far too early at the age of 38, leaving his wife Diana and their children Aleksandra (5 years old) and Michael (2 years old) in deep sadness. It's too unfair, so young, so full of life, why did he have to fall there, that day? We share this incomprehension with his family. This tragedy leaves an immeasurable void.
>
>Evgeny will remain in our memories as a symbol, that of road insecurity which threatens all vulnerable users. This is not inevitable, this situation must be improved to save lives. There is still so much to do to make the roads safe enough for cyclists and also pedestrians.
>
>We know that roundabouts often generate danger, but it must be possible to put in place arrangements providing better protection and moderation of motorized traffic to reduce the risk. We urge the communities in charge of the road network to urgently implement effective solutions, so that such an unacceptable tragedy does not happen again.
>
>We also urge everyone to rethink their way of getting around on a daily basis, to measure their responsibility in this common space that is the roads. Evgeny's choice to travel by bike, a simple and silent means of transport, good for our health and for the quality of our air, remains a model to follow whenever possible for everyday travel. On this roundabout, Evgeny was a courageous pioneer operating in a hostile environment. We hope for a peaceful and secure road, on which the choice made by Evgeny could be more widely followed.
>
>Dear all, a big thank you for joining this ceremony, a big thank you for bringing a little comfort to Diana and her children.
>
>We will now proceed to the installation of the white bicycle in memory of Evgeny. This white bicycle prepared by the “Pau à Vélo” association joins the already numerous others which have been left in several places in our town. They remind us of the risks that motorized traffic poses to vulnerable users, they are like a message to all drivers: slow down, look, move differently, life is so precious!
>
>Thank you again for your presence.

Cet hommage a fait l'objet d'[un article dans la république des pyrénées](https://www.larepubliquedespyrenees.fr/pyrenees-atlantiques/pau/pau-un-hommage-rendu-au-cycliste-mort-au-rond-point-delaunay-18296904.php). Ce journal a également relaté nos démarches et notre exaspération dans [un autre article](https://www.larepubliquedespyrenees.fr/societe/vie-quotidienne/circulation/combien-d-accidents-mortels-faudra-t-il-pour-securiser-les-parcours-cyclistes-et-pietons-demande-pau-a-velo-16822297.php)
