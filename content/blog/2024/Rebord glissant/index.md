---
title: "Rebord glissant"
date: 2024-01-14T17:00:00+01:00
author: "Aurélie Erb"
tags:
  - action
  - 14 juillet
  - sécurité
---

Suite à de très nombreux accidents, plusieurs par jours dont certains très graves, sur les nouvelles pistes cyclables à l'entrée du pont du 14 juillet, rue marca à Pau, l'association a décidé de signaler la dangerosité des lieux.

{{< gallery "photos" >}}

Le choix de séparer la chaussée, piste et trottoir par de la pierre d'Arudy, certes très esthétique, se révèle au final extrêmement glissant, et ce  quelque soit la vitesse ou la météo. Les roues de vélos glissent sur la pierre entraînant des dizaines et des dizaines de chutes. Hospitalisations, pompiers, abandon de la pratique du vélo, choix d'autres itinéraires, blessés avec séquelles. Voilà les conséquences de ce séparateur. 

Nous avions alerté la ville lors de nos réunions mensuelle l'an dernier, mais le problème ne semble sans solution pour eux jusqu'à aujourd'hui. A défaut, Pau à vélo informe les cyclistes de la dangerosité des lieu par une signalétique au sol bien visible.

