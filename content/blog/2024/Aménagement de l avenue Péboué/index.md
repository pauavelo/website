---
title: "Aménagement de l'avenue Péboué"
date: 2024-09-21T17:55:18+01:00
author: "Julien Garnier"
tags:
  - Péboué
  - analyse
  - aménagements
---

Avenue Péboué : un aménagement tout simplement… raté !

Malgré de longues discussions en 2022, nous n’avons pas réussi à infléchir les choix de la mairie qui a décidé de réaliser une voie verte en faisant cohabiter piétons et cyclistes. Soit…

<div class="gallery">
{{< image src="images/peboue2.jpg" caption="" width="" >}}
</div>

Mais au-delà de la cohabitation piétons/vélos qui n’est jamais souhaitable, ce qui est immensément regrettable ici, c’est la mise en œuvre de cette voie verte : tout semble avoir été fait pour décourager les cyclistes de l’emprunter :

- chaque passage devant une entrée de maison se fait avec un abaissement du niveau de la voie verte. Il y a 30 entrées de maison longeant la voie verte, ce qui la transforme ainsi en montagne russe (ce n’est pas la cata mais ce n’est pas non plus agréable…)

<div class="gallery">
{{< image src="images/peboue1.jpg" caption="" width="" >}}
</div>

- le revêtement est en sable stabilisé, ce qui assurera aux cyclistes d’arriver maculés au boulot les jours de pluie. Rappelons que l’utilisation de revêtement stabilisé n’est pas moins émettrice de CO2 puisqu’il contient du ciment. Par ailleurs le stabilisé laissant place au bitume à chaque entrée de maison, un ressaut se créera immanquablement à chaque transition de revêtement.
Pourquoi, en 2024, réaliser un aménagement qui soit si contre-productif ? D’autant que nous avions pris soin, bien avant les décisions d’aménagement, de faire des préconisations sur la mise en œuvre pratique.
Pourquoi, en 2024, continuer à fournir aux cyclistes des aménagements qu’on n’imaginerait même pas proposer aux automobilistes ? Vraisemblablement parce qu’à Pau, on ne considère par le vélo comme un moyen de déplacement qui a pleinement sa place dans la mobilité des 15 prochaines années.


<div class="gallery">
{{< image src="images/peboue3.jpg" caption="" width="" >}}
</div>

C’est extrêmement décevant et les conséquences seront multiples :
- les cyclistes utilisant actuellement l’avenue Péboué, n’emprunterons pas ou peu cette voie verte : imagine t’on proposer aux automobilistes palois une rue refaite à neuf, en gravier avec 30 ralentisseurs ? Par conséquent, ces cyclistes ne bénéficieront pas de la sécurité que pourrait apporter la circulation sur cette voie verte. De plus les automobilistes ne comprendront pas pourquoi les cyclistes n’empruntent pas la voie verte, entrainant une nouvelle fois un conflit d’usage.
- n’étant pas un aménagement qualitatif et efficace, certaines personnes ne franchiront pas le cap du déplacement à vélo.
Espérons au moins que cette réfection de l’avenue Péboué répondra aux attentes des riverains, à savoir une réduction du trafic et des vitesses des voitures. Mais avec un bitume refait à neuf sur une avenue rectiligne de 1 km de long et sans modification du plan de circulation, il est peu probable qu’un miracle se produise.




