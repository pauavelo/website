---
title: "Hommage à Azizullah"
date: 2024-02-10T22:42:37+01:00
author: "Sébastien Lamy"
tags:
  - hommage
  - accident
  - rond-point de la Commune de Paris
  - médias
---

Ce samedi 10 janvier, notre association invitait le public à rendre hommage à Azizullah Niazi, cycliste livreur décédé le 20 décembre 2023 des suites du dramatique accident survenu le 05 décembre 2023 près du giratoire de l'université , avenue Dufau à Pau.

Nous avons déposé un vélo blanc sur ce lieu, et plusieurs discours ont été prononcés. Cette cérémonie a été un moment de recueillement et un symbole de soutien envers les proches du jeune Azizullah.

{{< gallery "photos" >}}

Voici le discours prononcé par notre association :

>
>Nous déplorons ce qui s'est passé ici le 5 décembre 2023. Azizullah a été percuté mortellement par une voiture, alors qu'il se déplaçait à vélo. À 22 ans, il était clairement trop jeune pour mourir.
>
>Notre association milite pour une ville où il est possible de se déplacer à vélo en sécurité, et cet accident nous met tristement face aux limites de notre action. Il nous rappelle aussi durement, s'il en était besoin, notre vulnérabilité.
>
>Nous nous déplaçons dans un environnement où chaque erreur, chaque inattention, de notre part, ou de la part d'un conducteur, peut entraîner un accident dramatique. Nous ne pouvons pas nous résoudre à l'accepter.
>
>Nous espérons que des mesures seront prises, que les comportements changeront pour que ce risque qui plane sur nos déplacements quotidiens disparaisse. 
>
>Nous espérons que les responsables politiques prendront toutes les mesures nécessaires pour protéger les cyclistes dans leurs déplacements.
>
>Nous invitons aussi chacun à repenser ses modes de déplacement et à utiliser le vélo tant que possible. Parce qu'à vélo on est moins dangereux, parcequ'à vélo le nombre fait la sécurité, et surtout parce que nous voulons d'un monde où chacun peut se déplacer à vélo sans avoir peur de mourir.

L'évènement a fait l'objet de parution dans les médias:
* [un article sur le site de la radio France Bleu](https://www.francebleu.fr/infos/faits-divers-justice/pau-un-hommage-rendu-a-azizullah-le-jeune-livreur-a-velo-afghan-ecrase-en-decembre-2490521)
* [un article dans le jounal "la république des pyrénées"](https://www.larepubliquedespyrenees.fr/pyrenees-atlantiques/pau/un-velo-blanc-a-la-memoire-du-jeune-afghan-decede-en-decembre-a-ete-depose-avenue-dufau-a-pau-18516711.php)
* [un reportage télévisé sur France 3 Pau Sud Aquitaine]()
