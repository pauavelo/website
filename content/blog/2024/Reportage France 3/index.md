---
title: "Reportage France 3"
date: 2024-01-12T20:00:00+01:00
author: "Sébastien Lamy"
tags:
  - médias
  - aménagements
  - Batsalle
  - Allées de Morlaàs
---

Ce vendredi 12 janvier, les reporters de France 3 ont profité de la venue d'[Olivier Schneider](https://twitter.com/oschneider_fub) (le président de la [FUB](https://www.fub.fr/), notre fédé nationale), pour faire un bilan en image des nouveaux aménagements qui se profilent à l'entrée est de Pau. L'occasion de féliciter des avancées, mais aussi de réfléchir à ce qui manque encore...
{{< youtube "cITWmEjNQ_0" >}}

Merci à France 3 pour ce reportage, et bravo à Aurélie, Antoine et Olivier pour leur intervention !
