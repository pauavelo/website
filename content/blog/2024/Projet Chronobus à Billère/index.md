---
title: "Projet Chronobus à Billère"
date: 2024-11-28T16:27:49+01:00
author: "Sébastien Lamy"
tags:
  - aménagements
  - route de Bayonne
  - Billère
  - dossier
  - analyse
---

La route de Bayonne à Billère, représentant la principale entrée Ouest de l’Agglo, fait l’objet d’[un projet global de réaménagement](hhttps://www.pau.fr/actualites/le-projet-damenagement-de-lentree-ouest-de-pau-decouvrir-en-video) mené par la communauté d'agglomération Pau Béarn Pyrénées (en abrégé CAPBP). Une concertation était ouverte du 4 juin au 12 juillet 2024, dont vous pouvez [télécharger le dossier](chronobus-a1-s2-dossier-de-concertation-vf.pdf). Notre association a apporté une contribution.


<div class="pure-g trombi">
{{< dossier id="chronobus-billere" title="Contribution à la concertation Chronobus" >}}
</div>



Cette contribution souligne le manque d'ambition concernant la diminution d'utilisation de la voiture, et des aménagements cyclables dont la continuité est interrompue à plusieurs reprises.

#### Plan de notre contribution

>Introduction
>1. Critique globale du projet et rappel des revendications de fond de l’association Pau à vélo
>2. II. Le Giratoire de la rocade Nord-Sud (Giratoire Jacques Chirac)
>3. Du giratoire de la rocade au parking-relais (P+R)
>4. Du P+R à la mairie de Billère
>5. De la mairie de Billère à la halle de Billère
>6. De la rue F.Heritier à la rue G.Bourgignon
>7. Rond-point Route de Bayonne / rue du Golf
>8. Sorties des propriétés
>9. Revêtements cyclables
>10. Conclusion


Le bilan de la concertation vient de sortir dans le courant de ce mois de novembre 2024. Vous pouvez le consulter ici.

<div class="pure-g trombi">
{{< dossier id="bilan-concertation" title="Bilan de la concertation" >}}
</div>

Il nous semble que l'avis de notre association est noyé au milieu des avis individuels des riverains et commerçants. On parle un peu de nous page 13, mais nos réticences sur les ronds-points sont minimisées. Autour, les autres contributions comportent beaucoup de demande de stationnement et de facilitation de la circulation en voiture, comme par exemple un tourne à gauche rue Françoise Héritier. Ces contributions nous semblent défavorable à la pratique du vélo : en général, les ronds points et file de tourne à gauche rendent plus difficile une pratique sécurisée du vélo, avec des traversées d'intersection plus compliquée à gérer. La limitation du stationnement automobile est quand à elle un vecteur de changement fort des pratiques de déplacement.

La conclusion de la concertation est équilibrée mais laisse entendre que pas grand-chose ne changera dans le projet.

Les travaux devrait commencer dès la fin 2025 avec une livraison estimée à début 2028.
