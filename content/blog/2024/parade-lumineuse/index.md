---
title: Parade lumineuse
date: 2024-12-13T19:00:00.000Z
author: Sébastien Lamy
tags:
  - animations
  - éclairages
---

Cette année encore, la parade lumineuse a été très réussie ! Spectacle de jongle pyrotechnique avant le départ, vélos illuminés de guirlandes (et parfois de torches...), et musique à fond ! Les [triporteurs du gave](https://letriporteurdugave.github.io/) étaient présents pour emporter les personnes qui ne pouvait pas pédaler.

De gros sourires sur les visages des participants et parmi tout le public croisé.

{{< gallery photos >}}
