---
title: "Cyclistes, brillez !"
date: 2024-01-09T16:38:16+01:00
author: "Sébastien Lamy"
tags:
  - animations
  - éclairages
  - sécurité
  - collège Jeanne d'Albret
  - collège Marguerite de Navarre
  - lycée Louis Barthou
  - lycée St Cricq
---

A l'occasion de l'opération "Cyclistes, brillez!" organisée par la FUB, des bénévoles de Pau à Vélo ont distribué gratuitement des éclairages rechargeables aux élèves cyclistes de 4 établissements la ville de Pau : les collèges Jeanne d'Albret et Marguerite de Navarre, ainsi que les lycées Saint Cricq et Louis Barthou. 

{{< gallery "photos" >}}

Cette opération a été l'occasion d'informer les jeunes cycliste sur l'utilité des éclairages pour leur sécurité. Elle a été très bien été accueillie par les responsables d'établissements et les élèves ravis de ces indispensables cadeaux. 

Pour rappel voici les éléments obligatoires pour la sécurité:
- 1 avertisseur sonore type sonnette
- feux de position avant (blanc ou jaune) et arrière (rouge).
- des catadioptres  (dispositifs rétroréfléchissants , jaune avant, rouge arrière et jaune sur les côtés)
- 2 freins (avant et arrière)
- gilet hors agglomération la nuit ou lorsque la visibilité est insuffisante
- casque pour les conducteur ou passager de moins de 12 ans
