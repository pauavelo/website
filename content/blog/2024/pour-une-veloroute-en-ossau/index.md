---
title: Pour une véloroute en Ossau
date: 2024-12-15T19:00:00.000Z
author: Sébastien Lamy
tags:
  - véloroute vallée d'Ossau
  - courrier
header: courrier.png
---

La Vallée d'Ossau organisait le 17 décembre un « Petit déj' de la voie verte ». Notre association n'ayant pas pu s'y rendre, nous avons co-signé une lettre à destination de la Communauté de communes de la Vallée d'Ossau. Nous vous la publions ici.

> Arudy, le 15 décembre 2024
>
> Monsieur le Président de la Communauté de Communes de la Vallée d’Ossau
> 
> Vous connaissez notre position concernant la Voie Verte de la vallée d’Ossau. Elle s’inscrit dans une réflexion générale sur les enjeux de la vallée d’Ossau, et pour un développement dynamique de la vallée compatible avec le respect de l’environnement et de la planète. 
>
> Nous sommes navrés que le beau projet d'une vraie Voie Verte en continu de Buzy à Laruns soit stoppé à mi-chemin sur les communes de Bielle et Gère-Belesten. Cette interruption suscite l’incompréhension voire la moquerie des usagers, locaux ou touristes, et des observateurs y compris dans la plaine. Partout ailleurs les décideurs ont mesuré le potentiel touristique des voies vertes en termes d’attractivité et de valorisation des territoires traversés. Chez nous hélas, c'est le blocage...
>
> Concrètement, si le tracé du projet d’origine est définitivement abandonné, les variantes voie verte le long du gave ou véloroute traversant les villages seraient à envisager. Encore devraient-elles assurer un cheminement continu, agréable, avec un revêtement de qualité et sans trop de dénivelé. Surtout elles devraient permettre de parcourir la vallée en toute sécurité, à l’abri des voitures… et des chiens errants tout aussi dangereux. 
>
> Dans l'espoir qu'une solution satisfaisante sera rapidement trouvée.
>
> Bien cordialement.
>
> * collectif Vallée Partagée
> * AF3V (Association Française des Voies Vertes et Véloroutes)
> * association Pau à Vélo (membre de la FUB et délégataire départemental de l'AF3V)

![](brebis-velo.jpg)