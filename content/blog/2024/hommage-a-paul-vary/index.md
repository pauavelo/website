---
title: Hommage à Paul Vary
date: 2024-10-19T15:00:00.000Z
author: FUB
tags:
  - Bayrou
  - violences motorisées
  - hommage
---
Mardi 15 octobre, Paul a été tué par un automobiliste après une altercation alors qu’il circulait à vélo. Il avait 27 ans. Il était membre de l’association Paris en Selle, et militait pour une ville où tout le monde puisse se déplacer en sécurité.  Nous sommes sous le choc, tristes, en colère. Nos pensées vont évidemment à la famille et aux proches de Paul. Aucun mot ne pourra atténuer leur douleur et leur profond chagrin. 

Ce drame résonne chez beaucoup d’entre nous. En tant que cyclistes, nous avons toutes et tous déjà été victimes de la violence motorisée : le coup de klaxon, les insultes, l’intimidation, les dépassements à brûle-pourpoint  voire plus... Cette violence motorisée est largement banalisée et tolérée par les pouvoirs publics. Aujourd’hui, cette violence motorisée tue. 

Nous avons rendu hommage à Paul ce samedi 19 octobre. Partout en France, la FUB, Paris en Selle, Mieux se Déplacer à Bicyclette et le Collectif Vélo Île-de-France appelaient à se rassembler à 17h45 pour une minute de silence devant chaque mairie. Cet hommage était un message à nos dirigeant·e·s : stop aux violences motorisées. Il est temps d’entendre la réalité de notre quotidien et de prendre toutes les mesures nécessaires pour éviter un nouveau drame !

Nous ne pouvons plus tolérer cette banalisation de la violence sur la route. Comme tout usager de la route, les cyclistes et les piétons, qui choisissent des modes de transport durables et respectueux de l’environnement, ont un droit fondamental à la sécurité. Ce droit ne devrait jamais être un sujet de débat ou une option mais une réalité incontestable. 

Il est impératif que le développement des aménagements cyclables ne soit plus perçu comme un choix optionnel, mais comme une priorité absolue. Piétons et cyclistes sont des usagers vulnérables qui doivent pouvoir se déplacer en toute sécurité. Nous avons besoin de politiques audacieuses, de projets concrets et d'engagements fermes pour protéger la vie de toutes et tous. Nous en appelons donc au Gouvernement et aux parlementaires pour qu’ils prennent des mesures concrètes et efficaces afin de garantir la sécurité dans nos rues et sur nos routes de toutes et tous, et en particulier des cyclistes et des piétons.

Lors du rassemblement à Pau, nous avons lu deux témoignages que nous avions reçu de cyclistes palois.

### Témoignage 1: 
> “Cette mort fait remonter en moi des souvenirs douloureux. Il y a quelques mois, alors que je roulais tranquillement, une voiture me double en ralentissant et le passager me hurle "ecologiste de merde vas te faire enculer". Que se serait-il passé si j'avais répliqué ? Il y a 18 mois, j'avais encore mon vélo de course, une voiture me double, se serre devant moi et pile. J'ai pu l'éviter de justesse et passer sur sa gauche, le chauffeur me hurle dessus sans que je comprenne ce qu'il disait car il bouffait en même temps un sandwich. Il m'a alors serré à gauche contre le rond point et, heureusement, j'ai pu tourner dans la rue alors qu'il allait tout droit. Alors oui il y a a Pau des gens qui jouent avec la vie des cyclistes." 

### Témoignage 2: 
> “Ce jour-là j'étais boulevard Alsace-Lorraine, il était 17h30 ; ça bouchonnait. Je remontais tranquillement la bande cyclable. Il faut faire attention à ceux qui coupent ce bouchon, piétons, vélo ou une voiture qui traverse. Je suis arrivée à hauteur d'un véhicule, un Monospace, bleu foncé délavé, qui s'était positionné sur la bande cyclable en la bouchant complètement. Il était à l'arrêt car le feu était rouge. Je suis descendue de mon vélo, me suis faufilée sur la droite de la voiture. Arrivée à la hauteur de la vitre passager, j’ai claqué ma main à plat sur la vitre. C’est sans doute idiot mais c’est une façon de dire : « et je suis là il y a une bande cyclable ! Tu as oublié ? » Je suis repartie sur mon vélo sur la bande cyclable. Je commençais à peine à prendre de la vitesse entre 5 et 10 km/h. La file de voitures s’est mise à avancer à nouveau car le feu était vert. Arrivé à ma hauteur le MonoSpace me percute volontairement et violemment de côté, puis repart pour traverser Oui le Carrefour. Je suis propulsée malgré ma faible vitesse face contre le trottoir, trois personnes témoins de la scène, crient alors que je suis à moitié assommée, « arrêtez-le ! Prenez la plaque d'immatriculation ! mais c'est dégueulasse, il est fou ce mec ! »
> 
> La suite de l'histoire : les pompiers , les flics, des heures aux urgences et un dépôt de plainte le lendemain.
>
> Six mois après j'ai reçu le résultat de ma plainte qui est classée sans suite car ils n'ont pas retrouvé la personne. Je me demande si réellement la police a fait des recherches. Les témoins avaient donné la description du monsieur et du véhicule et la moitié de la plaque d'immatriculation. 
>
>Aujourd’hui après quelques séances chez un psychologue je n’ai plus peur sur mon vélo et je travaille encore ma zen attitude. Mon genou et mon coude gauche, mon nez et mon front ont guéri. Pas de trauma crânien grâce au casque. Il me reste un souvenir, puisque les tendons de mon épaule ne veulent pas guérir.”

Une centaine de personnes étaient présentes; dont le maire de Pau et quelques élu.es.

{{< gallery photos >}}

Notes: Notre texte reprend en grande partie les éléments du [communiqué de la FUB](https://www.fub.fr/fub/actualites/paul-disons-stop-violence-motorisee)