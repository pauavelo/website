---
title: Les idées mènent le monde
date: 2024-11-30T15:00:00.000Z
author: Aurélie Erb
tags:
  - action
---

Pour les 10 ans des rencontres littéraires "les Idées mènent le monde" l'association Pau à vélo a marqué le coup en répondant à la question de cette année "Est-ce ainsi que les Hommes vivent ?".

Devant le parterre d'automobiles publicitaires qui semble mener le salon qui mène le monde, l'association a fait un happening avec des messages sur l'image véhiculée par toutes ces voitures à l'entrée du palais Beaumont. Entre autres: 

> Est-ce en voiture que les Hommes vivent?

> Les publicités automobiles mènent le monde 

![](banderoles.jpg)


En tout cas le public n'a pas été entièrement conquis par cette campagne de promotio : beaucoup étaient venus en vélo. Quel succès !

{{< gallery velos >}}

 Ceux qui avaient choisi la voiture n'ont quant à eux pas hésité à squatter les trottoirs en toute impunité, malgré la navette qui était disponible depuis verdun notamment.

![](stationnement-trottoir.jpg)

![](navette.jpg)



Cependant **l'association salue les progrès réalisés par la ville de Pau** :
* le parc Beaumont n'est plus transformé en parking
* Des arceaux vélos ont été rajoutés pour l'évènement, ainsi que des barrières pour attacher les vélos (le public n'a pas trop compris l'objectif des barrières)  
  ![](arceaux.jpg)
  ![](barriere.jpg)
* une navette spéciale est mise en place  
* une communication sur les différents modes de transports
* des vélos électriques sont mis à disposition des invités qui le souhaitent.  
  ![](invites.jpg)

 
On y croit pour l'année prochaine ! 
* Un parvis libéré de la publicité automobile
* Des trottoirs libérés du stationnement sauvage.