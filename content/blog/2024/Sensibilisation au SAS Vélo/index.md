---
title: "Sensibilisation au SAS Vélo"
date: 2024-11-17T17:41:22+01:00
author: "Michel Rietsch"
tags:
  - action
  - sas vélo
---
L'association s’est mobilisée pour mettre en évidence les défaillances des institutions en matière d'aménagement cyclable et d'entretien du marquage au sol (sans parler du nettoyage).

{{< gallery "photos" >}}


Cette fois l'association est intervenue sur **le carrefour où Léa Viu a perdu la vie** renversée par un camion alors qu'elle se déplaçait à vélo en mai 2017. Exceptée la branche Nord du carrefour, le marquage au sol y était grandement effacé, dont passages piétons et sas vélo. Avec absence complète de ce dernier pour la branche sud.

Un rafraîchissement ponctuel et artistique des peintures de signalisation au sol a été réalisé par l'association à ses frais et risques.

Le maire de Pau et président de la CDA s'étaient engagés à faire respecter les règles.  Ils étaient également présents lors de l’hommage à Paul Vary le 19 octobre. Comment se peut-il que les marquages soient effacés? 

Un grand nombre d'automobilistes ignorent à quoi correspond un sas vélo, y compris les forces de l'ordre pour partie. Ou tout du moins pour ces dernières le niveau d'infraction que cela constitue de chevaucher un sas vélo (135€, 4 points).

Le message peint par l'association a été adapté en conséquence, avec un clin d'oeil au peintre surréaliste René Magritte

![](magritte-pipe.jpg)

La police municipale, lors d'une autre action de Pau à Vélo en juin 2024, a rappelé aux peintres improvisés que la mairie est la seule légitime pour le faire. Qu'attend-elle pour agir écrivait-on alors? On attend toujours!
