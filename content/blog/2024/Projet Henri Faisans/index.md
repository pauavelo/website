---
title: "Projet Henri Faisans"
date: 2024-01-09T22:04:42+01:00
author: "Victor Dorland"
tags:
  - proposition
  - Henri Faisans
  - aménagements
  - chronobus
  - dossier
---

En décembre, la ville a présenté son projet de requalification de la rue Henri Faisans.

Sachant à quel point la rue Henri Faisans est un tronçon clef pour l'accès au centre-ville des vélos et bus, nous sommes tombés de haut :

- Un projet peu ambitieux qui ne change rien au partage de la voirie, sauf la suppression de quelques stationnements pour planter des arbres
- Aucune contrainte au trafic automobile en transit
- L’absence totale de site propre pour les bus malgré l’octroi d’une subvention pour le Chronobus représentant 20% du coût des travaux
- L’absence absolue d’aménagement cyclable au mépris de l’Article L228-2 du Code de l’Environnement provoquant ainsi une rupture de l’axe cyclable d’entrée de ville depuis Idron

Afin, que ce projet soit en accord avec les enjeux d’aujourd’hui et de demain, Pau à Vélo propose les mesures suivantes :
- Fermeture du Cours Bosquet à la circulation sauf bus et vélo afin de stopper le trafic de transit
- Refonte du plan de circulation de la rue Henri Faisans dont l’accès et le stationnement sont réservés aux riverains
- Ajout d’une piste cyclable bidirectionnelle en lieu et place d’une file de stationnement (cf. profil ci-dessous)

Le dossier complet dressé par Pau à vélo a été transmis à la ville lors de la phase de consultation, vous pouvez le consulter ici :

<div class="pure-g trombi">
{{< dossier id="henri-faisans" title="" >}}
</div>
