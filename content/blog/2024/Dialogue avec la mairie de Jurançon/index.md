---
title: "Dialogue avec la mairie de Jurançon"
date: 2024-10-23T17:55:18+01:00
author: "Julien Garnier"
tags:
  - Jurançon
  - dialogue mairie
---

Clap de fin pour le recours contentieux que l’association avait déposé au tribunal administratif contre la mairie de Jurançon

Initié il y a maintenant 2 ans, l’association avait choisi ce mode d’action car plus aucune discussion n’était possible avec les élus de Jurançon. La communication était rompue depuis les municipales de 2020 alors même que l’équipe avait répondu au questionnaire de la FUB pour connaitre les intentions des différentes listes.

L’absence de communication ainsi que l’absence de mise aux normes des doubles sens cyclables sur le périmètre de Jurançon a amené les administrateurs de l’association à s’engager dans un recours gracieux, puis, en l’absence de réponse à celui-ci, dans un recours contentieux.

Après plusieurs rebondissements durant l’instruction de ce recours, une rencontre entre 2 administrateurs de l’association, le maire, l’élu aux mobilités et le responsable des services techniques a eu lieu en juin de cette année. Ce fut l’occasion de mettre à plat les différends, d’expliciter les projets de la commune et pour Pau à vélo de passer une nouvelle fois le message qu’une commune comme Jurançon a tout à gagner à organiser un plan de circulation contraignant pour les voiture, afin d’une part de réduire la trafic de transit (déploré par le maire lui-même) et d’autre part afin de ne pas avoir à investir de l’argent public dans des aménagements cyclables dans des rues qui sont censées recevoir un trafic très limité uniquement dû aux riverains.

A l’issue de cette rencontre, les administrateurs de l’association ont décidé d’abandonner le recours contentieux, la mise aux normes des doubles sens cyclables étant programmée pour 2024 (actuellement en cours de déploiement).

Deux leçons principales retenues de cette expérience :

* Les petites communes sont très mal informées sur les réglementations et aménagements spécifiques au vélo. L'association est toujours prête à les accompagner, mais nos actions sont mal connues et bien souvent vues comme des entraves aux aménagements envisagés.
* Le recours gracieux est simple à déposer. Le recours contentieux est quant à lui beaucoup plus compliqué à mettre en œuvre pour être pertinent et nécessite forcément de se faire accompagner par un avocat. L'association a eu la chance d'être secondé par un avocat très compétent, sans l'aide duquel nous n'aurions pas pu rédiger les mémoires déposés durant la procédure.


