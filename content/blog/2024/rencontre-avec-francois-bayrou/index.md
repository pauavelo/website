---
title: Rencontre avec François Bayrou
date: 2024-05-13T17:00:00.000Z
tags:
  - aménagements
  - atelier vélo
  - Bayrou
  - rond-point
  - rues scolaires
  - REV
author: Antoine Lefeuvre
---
Avec beaucoup de retard, voici le compte-rendu de notre rencontre, aux côtés de la FUB et de l'Atelier Vélo, avec François Bayrou. Le premier des engagements issus de cette importante réunion est devenu aujourd'hui réalité. Mais, n'allons pas trop vite, **revenons d'abord au 12 janvier**.

Ce jour-là, après moults rebondissements et un gros travail de préparation en amont, Aurélie Erb, Sébastien Lamy et moi-même, administrateur·trices de Pau à Vélo, étions reçus par le Maire dans une salle comble. Étaient notamment présents Jean-Louis Péres, 1er adjoint aux Finances, Michel Capéran, adjoint à l'Urbanisme, Christophe Colombel, le directeur général des services de Pau et de l'Agglo, ainsi que d'autres élus et responsables de services. De notre côté, **Olivier Schneider, président de la Fédération des Usagers de la Bicyclette, nous faisait l'amitié de revenir à Pau**, et Serge Deloustal représentait l'Atelier Vélo Participatif et Solidaire.

Les échanges ont duré plus d'une heure. La Ville a défendu son bilan, la FUB a clairement expliqué ce qui était nécessaire pour mettre en place un [système vélo](https://www.cc37.org/aux-origines-du-systeme-velo-par-frederic-heran-fub/) (notamment un Réseau Express Vélo) et Pau à Vélo avons cherché à obtenir un maximum d'engagements de la part de François Bayrou pour la seconde partie de son mandat.

### Les engagements du Maire

Voici ce que nous avons obtenu, dans un relevé de décisions rédigé par la Ville :

« La collectivité, Pau à Vélo et la FUB s'entendent sur :

- L'intérêt et la sollicitation d'un appel à projet national à destination des collectivités de taille moyenne (via le fonds > "mobilités actives") pour le développement des Réseaux Express Vélo (REV).
- L'étude d'un scénario alternatif sur la rue Henri Faisans, prévoyant un aménagement cyclable dédié.
- L'objectif permanent de pacification des rues pour réduire les circulations de transit et redonner de la place aux modes actifs.
- La recherche d'un local de 50m² avec une façade visible, pour héberger l'Atelier Vélo Participatif et Solidaire (AVPS).
- La différenciation des fonctions de certaines rues dans le temps, avec l'expérimentation de rues scolaires dans le cadre du Challenge de la mobilité inter-écoles (mai 2024).
- L'importance de mettre en œuvre les mesures nécessaires pour réduire l'accidentologie au niveau des ronds-points (y compris ceux de la rocade), et de certains carrefours.
- L'aménagement dédié et sécurisé des différents axes permettant de rejoindre et de quitter le centre-ville de Pau (sud de la rue Carnot et alentours des Halles, rue Henri Faisans, …). »

### Et depuis ?

Concernant la **rue Henri Faisans**, des scénarios alternatifs prévoyant des aménagements cyclables nous ont été présentés à l'occasion de réunions dédiés en février. Nous avons communiqué [la position de l'association](https://e.pcloud.link/publink/show?code=XZCUgMZ0VC3HcipdDj24qb4Uz0vnj3RzArV) et, depuis, les études sont toujours en cours (dernier renseignement pris le 30 avril).

[Les premières rues scolaires](https://www.larepubliquedespyrenees.fr/pyrenees-atlantiques/pau/pourquoi-des-rues-a-pau-seront-interdites-a-la-circulation-la-semaine-prochaine-19602628.php) ont fait leur apparition aujourd'hui dans trois écoles paloises (Bosquet, Pierre et Marie Curie et Jean Sarrailh), et aussi à l'école Jean Moulin de Jurançon. Il s'agit d'une expérimentation dans le cadre du challenge de la mobilité inter-écoles. Nous resterons mobilisés pour que ce dispositif soit amplifié et pérennisé, mais c'est une première belle victoire après deux ans de demandes insistantes de notre part et la nécessité de faire collaborer plusieurs services (mobilités, éducation, sécurité publique, communication).

Concernant l'**accidentologie au niveau des ronds-points**, nous en avons discuté avec la Ville lors d'une réunion en avril. Après nous avoir présentés un bilan chiffré et géolocalisé de l'accidentologie des cyclistes (un chiffre à retenir : dans 93% des cas, le cycliste n'est pas en tort), nous avons été informés des premières mesures, qui sont clairement cosmétiques (marquage au sol). Nous avons donc demandé des mesures efficaces comme, par exemple, sur l'axe Fébus, la mise en place de la priorité cyclistes et le passage des ronds-ponts de trois à deux voies. Des arbitrages doivent être rendus.

Enfin, concernant le tronçon de la **rue Carnot** jouxtant les Halles, un projet d'inversement du sens de circulation est dans les tuyaux, ce qui la rendrait quasi piétonne et bien plus cyclable. Affaire à suivre.

Beaucoup reste ainsi à faire pour que ces engagements se concrétisent, mais vous pouvez compter sur notre détermination. Si vous souhaitez d'ailleurs y contribuer, n'hésitez pas à participer au groupe Aménagements.