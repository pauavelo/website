---
title: "Voyage à Arthez de Béarn"
date: 2024-06-02T15:07:51+02:00
author: "Pierrick Hospital"
tags:
  - voyage
  - véloroute v81
  - Arthez de Béarn
  - camping
  - Cami Salié
---

Nous étions 22 cyclistes au départ de Pau


{{< gallery "depart" >}}

Pique-nique à Tarsacq vers 12h20. Super coin !

{{< gallery "tarsacq" >}}

Arrivés à Artix vers 14h45 samedi après-midi. 
Joelle et Jany nous ont laissés et sont reparties à vélo par le même chemin.

* 22-2 = 20 cyclistes restants

Emmanuelle, Fafa, Fred, Olivier et Christine ont été boire un coup pendant qu’on faisait les courses (grillades + pâtes pour le soir)

{{< gallery "tarsacq-arthez" >}}

Arrivés au camping à 15h55


{{< gallery "arthez" >}}

Delphine et Lionel sont repartis par le même chemin. Christine les a accompagné jusqu'à Artix où elle a pris le train.

* 20-3 = 17 cyclistes restants

Patrick a pu profité du lac voisin pour aller pêcher un coup. La plupart d'entre nous sommes allés au bourg d'Arthez en montant à pied, pour profiter ensuite d'une bière en terrasse (moment très sympa!), suivi d'une visite du village avec de jolis points de vues. Merci à Paul et Patrick qui ont géré le barbecue au retour, merci à Françoise d'avoir géré les pâtes, et merci à Julien et les autres pour la vaisselle ! Dans la soirée, certains ont profité de la boîte à lire du camping, pleine de BD


La nuit au camping s'est finalement avérée assez bruyante, de nombreux voisins ayant décidé de faire la fête jusque tard.

Le dimanche matin vers 9h30, Emmanuelle, Fafa, Fred et Olivier sont repartis en avance à Artix pour laisser les dames prendre un train vers Pau, puis ils ont continués seuls vers Pau.

* 17-4 = 13 cyclistes restants

Pendant notre petit déjeuner, il s'est mis à pleuvoir, mais nous avons vite été rassurés, au moment du départ à 10h15 la pluie s'était déjà arrêtée, nous ne l'avons plus revue.

À la pause, vers 11h15, Frédérique et Paul sont partis devant car Paul avait mal au dos et qu’ils voulaient pédaler plus vite que le groupe.

* 13-2 = 11 cyclistes restants

Arrivés proches de Artix, Élisa, Julien et Louise sont partis de leur côté vers la gare pour rentrer en train.

* 11-3 = 8 cyclistes restants

Pique-nique à midi à Labastide-Monréjeau

{{< gallery "labastide-monrejeau" >}}

Café vers 13h45 au zoo de Lescar, ambiance sympa !

{{< gallery "arrivee-lescar-pau" >}}

À l’intersection entre le Cami salié et la Paris-Madrid, Françoise a pris la direction de chez elle

* 8-1 = 7 cyclistes restants

Arrivés au crayon du Leclerc vers 15h00. Aucune panne de vélo, tout le monde avait l’air content !

Voici le parcours suivi

<p><a href="https://www.openrunner.com/route-details/19130310" target="_blank"><img src="parcours.png" title="Ouvrir le parcours sur openrunner"></a></p>
