---
title: Les lavandières à vélo
date: 2024-04-11T21:02:32.353Z
lastmod: 2024-06-03T21:00:00.000Z
author: Nicolas Laborie
tags:
  - aménagements
  - Bizanos
  - collège des Lavandières
  - Idron
  - Léé
  - proposition
  - Savoir Rouler à Vélo
  - école des Platanes
  - école primaire de Léé
  - D802
---
En lien avec l’étude de l’ADEME portant sur la mobilité à vélo des collégiens et lycéens, l’association Pau à Vélo porte le projet d’une desserte du collège des Lavandières, qui est le collège du secteur de Bizanos, Idron et Lée. Plus de 40% des 600 collégiens proviennent d'Idron et Lée. Nous proposons donc la création d'un itinéraire cyclable sécurisé depuis l'école primaire de Léé jusqu'au collège de Bizanos, en passant par l'école des platanes d'Idron. Retrouvez ici le dossier complet de notre proposition à télécharger.

<div class="pure-g trombi">
 {{< dossier id="lavandieres" title="Les lavandières à Vélo" >}}
</div>

Au sommaire :

* Présentation du projet « Les lavandières à vélo »
* Description préliminaires des travaux
* Projets existants dans la zone et avis de Pau à Vélo
* Avantages complémentaires du projet
* Propositions d'actions

Ce dossier pdf inclus également une lettre de soutien du maire de Bizanos.

Nous avons rencontrés plusieurs interlocuteurs pour leur présenter ce projet
- Ville de Pau et Communauté de Communes de l'Agglomération de Pau, rencontrés le 11 avril 2024, 
- Ville de Bizanos, rencontrée le 12 février 2024, 
- Ville d’Idron, rencontrée le 4 mars 2024, 
- Ville de Lée, rencontrée le 13 février 2024, 
- DSDEN, rencontrée le 8 avril 2024

D'autres acteurs doivent être rencontrés pour leur présenter le projet :
- Conseil Départemental des Pyrénées Atlantiques 
- Conseil Régional de la Nouvelle Aquitaine

**Edit du 19/04/2024** : Nous avons également reçu un [soutien du centre culturel et de loisir (CCL) de Léé](avis-ccl-de-lee.pdf)

**Edit du 03/06/2024**: Nous avons également reçu le [soutien de l'association des parents d'élèves d'idron](avis-ape-idron.pdf)


