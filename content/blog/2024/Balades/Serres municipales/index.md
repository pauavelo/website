---
title: "Serres municipales"
date: 2024-05-05T14:23:10+02:00
author: "Pierrick Hospital"
tags:
  - balade
  - serres municipales
  - luy
  - forêt de Bastard
---

Ce dimanche 5 mai, sommes allés faire un tour au parc de Sers à l'occasion d'une [journée portes ouvertes](https://www.pau.fr/actualites/amenagez-un-jardin-deau-samedi-20-avril-au-parc-de-sers).

La météo était très clémente.

![](groupe.jpg)

Le parcours a été tout simple à travers la ville (moins de 8km).

<p><a href="https://www.openrunner.com/route-details/18911623"><img src="parcours.jpg" title="Ouvrir le parcours sur openrunner"></a></p>



Une fois arrivé, nous avons pu visiter les serres municipales où poussent les plantes que l'on voit ensuite dans les rues de Pau. Quel dommage qu'on ne puisse pas y trouver aussi des racks à vélo pour se garer !


{{< gallery "serres" >}}

Plusieurs animations étaient prévues : Déclamations théâtrale, explications et conseils des jardiniers, stands, tours en calèche, écriteaux et affiches. Il y avait également un petit parc attenant avec un labyrinthe végétal, composé de nombreuses plantations à regarder, toucher ou même goûter (le labyrinthe des cinq sens).

Après la visite, comme nous pouvions attendre le repas, nous avons fait une petite boucle jusqu'au Luy du Béarn et la forêt de Bastard avant de prendre le chemin du retour.

{{< gallery "pres-du-luy" >}}

Nous étions de retour à notre point de départ vers 13h.


