---
title: "Petit tour en ville"
date: 2024-01-07T18:54:32+01:00
author: "Pierrick Hospital"
tags:
  - balade
---

En ce début d'année, le temps était très froid, mais nous somme quand même allé faire un tour à vélo pour nous aérer.

![](img-0389.jpg)


Nous vous proposions une petite balade en ville privilégiant les pistes cyclables et les rues calmes.

Cette petite boucle faisait 9km et il n'y avait pratiquement pas de dénivelé.

<p><a href="https://www.openrunner.com/route-details/18149332"><img src="parcours.jpg" title="Ouvrir le parcours sur openrunner"></a></p>


![](img-0391.jpg)



