---
title: "Cueillette de l'Aragnon"
date: 2024-09-01T10:00:00+02:00
author: "Pierrick Hospital"
tags:
  - forêt de Bastard
  - cueillette de l'Aragnon
  - balade
---

Ce dimanche, nous étions peu de cyclistes au départ de la balade pour aller à la cueillette de l’Aragnon.
Sans doute les gens ont-ils eu peur de la pluie? C’est dommage car nous ne l’avons pas du tout eu.
Nous avons traversé tranquillement la ville du sud au nord, puis la forêt de Bastard.

<p><a href="https://www.openrunner.com/route-details/19823315" target="_blank"><img src="parcours.jpg" title="Ouvrir le parcours sur openrunner"></a></p>


Nous sommes arrivés à la cueillette vers 11h10.
Ceux qui le souhaitaient ont eu le temps de récolter des salades, tomates et courgettes…
D’autres ont simplement pris un café.
Puis, après avoir rangé nos victuailles, vers 11h50, nous sommes repartis vers Pau.

Ça a été l’occasion de se rendre compte que les vélos font des bruits étranges lorsqu’ils ont besoin d’être révisés.

C’était une chouette matinée bien agréable somme toute.


{{< gallery "photos" >}}

À la prochaine.
