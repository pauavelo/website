---
title: "Balade convergente"
date: 2024-10-06T10:00:00+02:00
author: "Pierrick Hospital"
tags:
  - balade
  - Artiguelouve
  - Assat
  - Zénith
  - Gan
  - véloroute v81
  - Gelos
  
---

Ce dimanche matin, il a fait beau pour la balade convergente.

Des 4 départs, c’est celui du Nord qui a eu le plus de succès avec une 15aine de cyclistes.
Au pont d’Assat, il y avait 4 cyclistes.
Au stade d’Artiguelouve, il y avait 2 cyclistes.
Sur la place de la mairie de Gan, il y avait 2 cyclistes.

![](parcours-convergent.jpg)


Nous nous sommes rejoints sur la plaine des jeux à côté du pradeau de Gelos pour le pique-nique.
À cet endroit, la véloroute a bénéficié d’une réfection de son revêtement.
C’est très bien fait et très agréable de rouler dessus.

![](2024-10-06-12-09-28.jpg)

En revanche, il y a eu plusieurs chutes ce dimanche matin.
La passerelle en bois qui enjambe le Néez derrière la station d’épuration est très glissante avec la rosée du matin.
Élisa et Louise en ont fait les frais, heureusement sans gravité.

Un couple de cyclistes peu expérimentés qui n’étaient pas avec nous, ont également chuté sur la passerelle en bois sur la véloroute 81 à Mazères-Lezons.
Il a fallu guider les pompiers pour qu’ils puissent arriver à l’endroit de la chute et les prennent en charge.

Soyez très prudents si votre itinéraire emprunte ces passerelles !
Le plus important est de conserver une trajectoire continue et de ne pas avoir à tourner le guidon sur le bois glissant.
Il est aussi recommandé de ralentir en arrivant sur la passerelle et de faire en sorte de ne pas avoir besoin, ni de freiner, ni de pédaler.

À bientôt,
