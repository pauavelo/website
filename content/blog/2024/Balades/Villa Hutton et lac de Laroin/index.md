---
title: "Villa Hutton et lac de Laroin"
date: 2024-02-04T19:16:44+01:00
author: "Pierrick Hospital"
tags:
  - balade
  - lac
  - villa
  - véloroute V81
---

Le week-end s'annonçait ensoleillé :sunny:, le temps idéal pour aller faire un tour à vélo. Mais finalement la balade s'est faite dans le brouillard :cloud:

{{< gallery "photos" >}}

Suggéré le mois dernier par Éric, nous sommes allé voir la villa Hutton. Uniquement de l'extérieur puisque c'est un bâtiment privé qui ne se visite pas.

Ensuite, nous avons descendu Billère par les Marnières jusqu'à la véloroute du long du gave, que nous avons suivi jusqu'au lac de Laroin.

Après un petit tour du lac, le retour s'est fait également par la véloroute V81.

<p><a href="https://www.openrunner.com/route-details/18345114"><img src="parcours.jpg" title="Ouvrir le parcours sur openrunner"></a></p>
