---
title: "Ville et forêt"
date: 2024-06-02T14:06:55+02:00
author: "Pierrick Hospital"
tags:
  - balade
  - forêt de Bastard
---

Les jours rallongent, on sent que l’été n’est plus très loin.
Pour ce premier dimanche de juin, la météo était très agréable.

Nous vous proposions une petite balade dans les rues de Pau entre autre sur des aménagements sécurisés: voies vertes, bandes et pistes cyclables. L’itinéraire était majoritairement urbain, mais il faisait quand même un petit tour dans la forêt de Bastard au nord de Pau.
Il faisait moins de 17km, avec pratiquement pas de dénivelé (sauf pour enjamber l’autoroute).
Comme d’habitude, le rythme était tranquille pour permettre à tous de profiter de la balade.

{{< gallery "photos" >}}

Voici le parcours

<p><a href="https://www.openrunner.com/route-details/19088046" target="_blank"><img src="parcours.png" title="Ouvrir le parcours sur openrunner"></a></p>


