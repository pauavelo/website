---
title: "Fête des cycles à Gan"
date: 2024-04-07T10:00:00+02:00
author: "Pierrick Hospital"
tags:
  - balade
  - véloroute vallée d'Ossau
  - Gan
---

Pour ce mois d'avril, nous étions invités par [Gan en Transition](https://ganentransition.wixsite.com/ganentransition) qui organisait leur première Fête des cycles.

![](fete-des-cycles-affiche.jpg)

Cette fête se voulait conviviale et se formalisait par des stands de sensibilisation sur différents cycles existants sur notre belle planète (par exemple l'azote, l'eau, le phosphore).

Et quand on dit cycle, on pense vélo bien évidemment !
Nous vous proposions donc d'y aller à vélo.

{{< gallery "photos" >}}

Sur place il y avait de la musique, des stands qui nous présentaient différents cycles (adoptant une approche souvent scientifique). L'atelier vélo participatif était également présent pour effectuer des réparations au pied levé.

Pour y aller, nous avons emprunté la véloroute de la Vallée d'Ossau, jusqu'au jardin verger partagé, chemin de la Ribère.

<p><a href="https://www.openrunner.com/route-details/18735410" target="_blank"><img src="parcours.jpg" title="Ouvrir le parcours sur openrunner" style="width:619px;"></a></p>


Certains sont restés pour profiter de la fête, et du temps superbe, propice à s'assoir sur l'herbe au bord du Neez. Pour le repas il fallait réserver, ce que nous n'avions pas anticipé. Du coup la petite troupe improvisée a prolongé jusqu'à Gan pour acheter des sandwichs.

![](img-0713.jpg)

Ceux qui ne souhaitent pas prolonger leur présence ont été raccompagné par Alexis, et ont pu retourner à Pau vers midi et demi.
