---
title: "Biodivélo à Bastarrous"
date: 2024-07-13T09:00:00+02:00
author: "Pierrick Hospital"
tags:
  - papillons
  - biodiversité
  - Gan
  - balade
---

Ce samedi 13 juillet, nous étions invités par Kévin Le Falher du Conservatoire des Espaces Naturels pour une «biodivélo».
C’était une balade à vélo à la découverte de la biodiversité autour de Pau.

Nous sommes allés sur une parcelle des côteaux de Gan où le CEN est en charge de surveiller et favoriser la population des rhopalocères (papillons de jour).

{{< gallery "photos" >}}


Nous étions une douzaine au départ à Billère. Le public était varié, certains avait entendu parler de la balade par le CEN, et d’autres par Pau à vélo.
Le temps était agréable, un peu nuageux, mais le soleil devait se découvrir dans la matinée.

Pour l’aller, nous avons longé le gave de Pau par la V81 jusqu’à Laroin pour remonter la vallée des Hiès où la pente est douce.
La dernière côte étant plus raide (~15%), certains ont du pousser le vélo.

<p><a href="https://www.openrunner.com/route-details/19310909" target="_blank"><img src="parcours.jpg" title="Ouvrir le parcours sur openrunner"></a></p>


Arrivés sur place, Kévin nous a présenté les lieux et nous a distribué des clés d’identification.
À l’aide de filets à papillons, nous avons réussi à capturer une dizaine de spécimens différents que nous avons conservés au fur et à mesure dans une cage adaptée, qui nous avons pu identifier ensuite. Certains se ressemble et ce n’était pas facile mais nous avons réussi avec l’aide de Kévin.

Après avoir relâchés les papillons, nous avons déjeuné notre pique-nique à l’ombre des arbres.
C’était l’occasion de discuter, notamment des actions de Pau à vélo.

Le soleil était maintenant bien dégagé, nous avons repris la route en direction de Gan.
Après la dernière côte, nous avons rejoint la véloroute de la Vallée d’Ossau jusqu’à Pau.
Le tour de France était déjà passé et nous n’avons pas été gênés par le peloton.

C’était une chouette journée.
C’est toujours un plaisir d’organiser des balades avec Kévin, qui est vraiment un expert des rhopalocères.

À la prochaine.


### Statistiques de la boucle à vélo

* Billère > Laroin > Gan > Jurançon > Billère
* Kilomètres effectuées : 28,12 km.
* Temps réalisée : 2h 08min.
* Vitesse moyenne : 13,2 km/h.
* Ascension réalisée : 213 m.
* 15 espèces de papillons de jour (rhopalocères) observées.
* 2 espèces de papillons de nuit (hétérocères) observées.
* 1 espèce de coléoptère observée.
* 1 espèce de cigale non déterminée observée. 

### Quelques liens utiles
* [Visualiser les données sur kollect](https://nouvelle-aquitaine.kollect.fr/index.php?module=observation&action=fiche&idfiche=282421) (base de données naturalistes du CEN)  
:arrow_right: Vous pouvez cliquer sur le nom des espèces pour avoir plus d'informations sur celles-ci (noms français, photos, localisations, etc.)
* [Site du Plan National d'Actions en faveur des papillons de jour](https://papillons.pnaopie.fr/accueil/) (documents, vidéos, actus, formations): 
* [Site du Conservatoire d'espaces naturels de Nouvelle-Aquitaine - partie animations à venir](https://cen-nouvelle-aquitaine.org/rendez-vous-nature/) : 
* Animations à venir, sur [le facebook du CEN](https://www.facebook.com/cennouvelleaquitaine/) 
* Base de données Kollect (utilisable par le CEN NA et par les particuliers sous forme web et application) : https://nouvelle-aquitaine.kollect.fr/
* [Base de données FAUNA](https://observatoire-fauna.fr/) (uniquement via site internet)  : 
* [Atlas des papillons de jour d'Aquitaine](https://leclub-biotope.com/fr/librairie-naturaliste/1742-atlas-des-papillons-de-jour-d-aquitaine)

