---
title: "Lac des Carolins"
date: 2024-12-01T10:00:00+01:00
author: "Pierrick Hospital"
tags:
  - balade
  - lac
  - Carolins
  - Paris-Madrid
  - Ousse des Bois
---

Belle journée ensoleilée de décembre (15°C quand même), les chemins étaient sec. Nous étions une vingtaine de cyclistes au départ, pas d'enfants cette fois ci. Après avoir distribué les étiquette de publicité pour notre balade lumineuse du 13 décembre, nous sommmes partis par des petites routes pour rejoindre le lac par la place Verdun, la Paris-Madrid puis les petits sentiers le long de l'Ousse des Bois.

{{< gallery "photos" >}}

Arrivés au lac, nous avons pris le temps de grignoter et de discuter, avant de repartir vers le centre-ville, par le Perlic nord, le Cami Salié, puis de nouveau la Paris-Madrid (mais un tronçon différent de celui pris à l'aller), et la piste cyclable de l'université.

<p><a href="https://www.openrunner.com/route-details/20338561" target="_blank"><img src="parcours.jpg" title="Ouvrir le parcours sur openrunner"></a></p>


Peu de circulation automobile sur le trajet, ce qui l'a rendu d'autant plus agréable !

Ce fut une belle sortie !
