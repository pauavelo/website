---
title: "Petite boucle à l'est"
date: 2024-03-03T10:00:34+01:00
author: "Pierrick Hospital"
tags:
  - Bizanos
  - Idron
  - balade
---

<p><a href="https://www.openrunner.com/route-details/18470221"><img src="parcours.jpg" title="Ouvrir le parcours sur openrunner"></a></p>


Cette fois, nous sommes allés vers l’est, pour une petite boucle en ville (Pau, Bizanos, Idron) où nous avons privilégié les pistes cyclables et les rues calmes.
Le parcours a fait une bonne douzaine de km, nous sommes descendus (puis remontés) au niveau de l’Ousse ce qui a fait 80m de dénivelé environ.
Nous sommes descendus par l’avenue Nitot et remontés par la rue des Frères Cousté à Bizanos, où la pente est douce et régulière.

Le soleil nous a salué au début de la balade, mais arrivé au stade du Hameau, la pluie et le froid nous ont encouragé à rentrer au plus vite sans faire de détour.

{{< gallery "photos" >}}
