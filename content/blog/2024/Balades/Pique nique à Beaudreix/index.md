---
title: "Pique nique à Beaudreix"
date: 2024-08-04T10:00:00+02:00
author: "Pierrick Hospital"
tags:
  - balade
  - Beaudreix
  - véloroute v81
---

Ce dimanche, nous étions une bonne douzaine à l’heure du rendez-vous au square Aragon pour aller pique-niquer.

<p><a href="https://www.openrunner.com/route-details/20104265" target="_blank"><img src="plan.png" title="Ouvrir le parcours sur openrunner"></a></p>


Nous avons pris le temps de faire connaissance. Puis nous sonnes descendus par les sentiers du Roy, passés devant la gare puis le stade d’eaux vives afin de franchir le gave par la passerelle de Gelos.
Désormais sur la véloroute V81, nous ne l’avons plus quittée jusqu’à Baudreix.
Nous avons pédalé tranquillement en profitant de la fraicheur (relative) de ce début de journée.

Au pied du pont d’Assat, nous avons essayé la toute nouvelle déviation qui permet d’éviter le rond-point de Narcastet mais nous n’avons pas été convaincus. Le sentier se révèle en effet bien trop raide. Il permet juste de rejoindre la route de Baliros où une piste bidirectionnelle semble se préciser.
À choisir, un itinéraire qui longe le gave pour rejoindre le bourg de Baliros aurait été beaucoup plus agréable mais ce n’est malheureusement pas la décision qui a été prise par le département.

Le trajet s’est passé sans encombres, nous sommes arrivés à destination vers midi et demi.

{{< gallery "photos" >}}

Nous avons mangé notre pique-nique sorti du sac sur l’herbe à l’ombre des arbres sur l’aire à côté du lac de Baudreix.
Nous avons aussi pris le temps pour un café et pour discuter des aménagements cyclables, de Pau, et d’ailleurs…

Le retour s’est très bien passé aussi.
Comme la route est globalement en faux-plat descendant, nous avons roulé un peu plus vite et nous sommes arrivés à Pau vers 16h.

C’était une belle journée ensoleillée. Il n’a pas fait trop chaud.
Les participant.es étaient ravi.es de la balade.


