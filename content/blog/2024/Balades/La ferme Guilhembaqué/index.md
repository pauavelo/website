---
title: "La ferme Guilhembaqué"
date: 2024-11-03T10:00:00+01:00
author: "Pierrick Hospital"
tags:
  - balade
  - véloroute v81
  - Laroin
---

Nous étions une quinzaine de cyclistes au départ au square Aragon.
Nous sommes d’abord partis vers le nord pour contourner le parcours de la Course Féminine avant de descendre la rue Marca, traverser le pont du XIV juillet et rejoindre la V81.
Puis nous avons pédalé tranquillement le long du gave. 

<p><a href="https://www.openrunner.com/route-details/20193587" target="_blank"><img src="parcours.jpg" title="Ouvrir le parcours sur openrunner"></a></p>


Nous sommes arrivés vers 11h15 à [la ferme Guilhembaqué](https://www.facebook.com/guilhembaque) au centre de Laroin. Yves nous y attendait.
Il nous a expliqué l’origine de ce tiers-lieu où de nombreux bénévoles participent à la vie associative.
Il nous a notamment expliqué les moyens utilisés pour rénover les différents bâtiments, ainsi que le fonctionnement des toilettes sèches.

{{< gallery "photos" >}}

Après les explications, quelques cyclistes sont repartis vers Pau.
La plupart d’entre nous avions apportés notre pique-nique que nous avons pu manger sur les tables devant la ferme.
Après un café et/ou tisane, nous avons également pu visiter l’étrange structure du zome.

Tout le monde a trouvé la visite intéressante. Nous reviendrons certainement.
Après avoir remercié nôtre hôte Yves, nous somme repartis vers Pau.
Pour changer de l’aller, nous avons bifurqué au niveau de la passerelle pour longer le golf, puis nous avons traversé le bois du château et le jardin de Basse Plante.
Nous nous sommes dit au revoir au pied du château.

C’était vraiment une belle journée!
