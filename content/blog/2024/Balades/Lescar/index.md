---
title: "Lescar"
date: 2024-07-07T10:00:00+02:00
author: "Pierrick Hospital"
tags:
  - Lescar
  - Paris-Madrid
  - Carolins
---

Nous sommes allés à Lescar. Nous n'y allons pas souvent, alors que c’est vraiment pas loin et tout à fait faisable à vélo.

La météo était plutôt fraiche pour un mois de juillet. L’itinéraire suivait un parcours en boucle d’une vingtaine de kilomètres. Il a été peaufiné pour éviter les axes avec beaucoup de motorisés, en empruntant des petites routes calmes et des sentiers. Nous sommes aussi restés sur les hauteurs, ce qui a limité le dénivelé à franchir.

<p><a href="https://www.openrunner.com/route-details/19388077" target="_blank"><img src="parcours.jpg" title="Ouvrir le parcours sur openrunner"></a></p>



Nous avions apporté de quoi grignoter, en particulier Odile qui nous a partagé ses excellente crêpes ! Arrivé à Lescar vers 11h30, nous avons parcouru le centre ville historique et sa cathédrale, et nous avons profité de la vue surplombante 

![](2024-07-07-11h48m57s-img-1572.jpg)
*(dommage, la vue sur les montagnes était bouchée par les nuages)*

En appuyant un peu sur la pédale au retour, nous étions revenus à notre point de départ vers 12h40.

{{< gallery "photos" >}}

