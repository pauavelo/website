---
title: Opération cycliste brillez pour les scolaires
date: 2024-11-18T16:08:34+01:00
author: Sébastien Lamy
tags:
  - animations
  - éclairages
  - médias
slug: operation-cycliste-brillez
---
A l'occasion de la rentrée scolaire de la toussaint, et dans le cadre de [l'opération "cycliste brillez" proposée par la FUB](https://www.fub.fr/evenements/cyclistes-brillez-2024), l'association Pau à Vélo a distribué des éclairages vélos aux collégiens et lycéens qui n'en disposaient pas.

{{< gallery "photos" >}}


Depuis le passage à l'heure d'hiver, beaucoup de déplacements à vélo s'effectuent pendant la nuit. Hors dans ce cas, les éclairages avant et arrière sont obligatoires sur les vélos, car ils constituent un facteur de sécurité essentiel. Sans eux le cycliste n'est pas assez visible par les autres usagers de la route.

Beaucoup de jeunes ignorent cette règle, ne prennent pas la peine de l'appliquer, ou n'ont simplement pas les moyens de le faire. L'association Pau à Vélo a donc mené une campagne de sensibilisation sur le thème de l'éclairage auprès des jeunes cyclistes de plusieurs établissements scolaires public : les collèges Jeanne d'Albret et Marguerite de Navarre, ainsi que les lycées Louis Barthou, St Cricq, et St John Perse.

Avec leur accord, l'association s'est présenté à la sortie de ces établissements pour informer les élèves à vélo de la nécessité d'être éclairé la nuit. À cette occasion, si les élèves le souhaitaient, ils pouvaient faire un bref contrôle des éléments de sécurité sur leur vélo (frein, état et gonflage des pneus, présence des éléments de sécurité obligatoires), et l'association "Pau à Vélo" leur a offert des éclairages s'ils n'en disposaient pas. Les éclairages proposés s'adaptaient aussi bien à l'avant qu'à l'arrière, et ils étaient rechargeable par USB.


Lors du passage à St Cricq, des journalistes de France Bleu et de La république des pyrénées sont venus couvrir l'opération.

{{< audio src="2024-11-19-france-bleu-pauavelo.mp3"
          caption="Extrait du journal de 8h du 19/11/2024 sur France Bleu" >}}

* [Reportage sur la république des Pyrénées](https://www.larepubliquedespyrenees.fr/pyrenees-atlantiques/pau/pau-a-velo-eclaire-les-cyclistes-22200170.php)

