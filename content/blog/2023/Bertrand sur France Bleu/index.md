---
title: "Bertrand sur France Bleu"
date: 2023-05-03T08:15:08+01:00
author: "Sébastien Lamy"
tags:
  - médias
  - conseils
---

Ce mercredi 3 mai 2023, Bertrand était l'invité de la rédaction au 6-9 sur France Bleu, interviewé par Mathias Kern pour représenter notre association. 

**Sujets abordés** : Est-ce que pau est une ville de vélo ? comment inciter à faire plus de vélo ? Le partage de la route ? Le code de la route pour le vélo ?

Vous pouvez retrouver l'interview sur [la page dédié du site de France Bleu](https://www.francebleu.fr/emissions/l-invite-de-la-redaction-de-france-bleu-bearn-bigorre/il-y-a-toujours-une-priorite-a-la-voiture-en-ville-constate-bertrand-ottmer-de-l-association-pau-a-velo-2443507), ou bien [la télécharger ici](2023-05-03-interview-bertrand-france-bleu.mp3)

Bravo à Bertrrand pour son intervention, et merci à France Bleu de nous avoir invité !
_____
Photo de l'article : Radio France, issue du site francebleu.fr
