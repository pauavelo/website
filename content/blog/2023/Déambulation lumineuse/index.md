---
title: "Déambulation lumineuse"
date: 2023-12-15T18:43:34+01:00
author: "Sébastien Lamy"
tags:
  - animations
  - balade
  - éclairages
  - vélorution
---

Ce dimanche 15 décembre, Pau à Vélo organisait une soirée festive !

Le rendez vous était donné place de la Reine Marguerite, ou un spectacle de jongle enflammé donnait le ton, tandis que chacun allumait lampion, guirlandes et torches pour faire briller leur vélo dans la nuit hivernale. 

{{< gallery "depart" >}}

Pour ceux qui n'avait pas d'éclairages "standard" pour leur vélo, c'était l'occasion d'en acquerir, car notre association en vendait à prix cassé.

La déambulation s'est élancée, en musique et sonnettes, dans une ambiance festive et joyeuse, à un rythme tranquille. Les plus de 90 personnes présentes avaient la banane !

![](2-deambulation-1.jpg)

![](img-0254.jpg)


Les triporteurs du gave étaient présents et transportaient une personne qui ne pouvait pas pédaler mais vouler profiter du spectacle...

La déambulation s'est terminée place royale. Ensuite, c'était resto et [ciné](/blog/2023/les-roues-de-lavenir/) pour ceux qui voulaient continuer la soirée...

![](3-place-royale-2.jpg)

![](img-0256.jpg)

![](4-resto.jpg)

{{< vimeo "908649513" >}}
