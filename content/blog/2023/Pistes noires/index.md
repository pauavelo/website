---
title: "Pistes noires"
date: 2023-06-30T12:49:56+01:00
author: "Marie Daviet"
tags:
  - action
  - aménagements
  - médias
---

Des pistes noires en ville ? A Pau, c'est possible.

Il y a quelques mois des stickers jaunes représentant un vélo ont fleuri sur certaines rues de l'agglomération paloise. 

![](sticker.jpg)

Objectif ? Cibler des pistes cyclables d'une difficulté élevée qui exigent une maîtrise avancée de son vélo, ou d’une piste comportant des périls qui exigent une vigilance hors norme.

Entre les pistes de «pump track» toutes neuves, les discontinuités, les nids de poules, les chaussées déformées ou rapiécées, sans parler des gravillons, stationnement mal placés, risques d'emportiérage,… vos fesses sont mise à rude épreuve !
Certaines de ces pistes sont réservées à ceux qui n'ont pas peur d'affronter slalom, trous et bosses aux quotidien, d'autres sont réservées aux cyclistes très expérimentés, à la fois agiles et extrêmement vigilants.

{{< gallery "photos" >}}

Les cyclistes de niveau intermédiaire pourront eux rouler sur la voie de circulation générale, souvent en meilleure état, mieux nettoyée et mieux pensée que les aménagements cyclables. Dans ces cas-là, il est conseillé de prendre la place sur la chaussée (comme dans une zone 30) pour dissuader les dépassements dangereux. Cela vous évitera d’avoir chaud aux fesses !

Si les pistes noires en ski font le bonheur de certains et certaines adeptes du ski, dans l'agglomération paloise elles freinent la pratique du vélo. Les problèmes des pistes cyclables que Pau à vélo a identifiées comme des pistes noires sont nombreux :
- discontinuités
- nids-de-poule
- chaussées déformées ou rapiécées
- gravillons
- risques d'emportiérage.

Actuellement, l'avenue Nitot, le boulevard Tourasse, l'avenue du château d'Este, l'avenue Philippon, le boulevard Alsace Lorraine ou encore la route de Bayonne sont considérées comme des "pistes noires". Pau à vélo espère et milite pour que ces pistes noires deviennent des pistes vertes accessibles à tous et toutes les cyclistes, peu importe leur niveau.

Cette action a été cité dans la presse
 * [La république des pyrénées - 31 août 2023](https://www.larepubliquedespyrenees.fr/economie/transports/a-pau-l-amelioration-des-pistes-cyclables-est-un-travail-de-longue-haleine-16386083.php)




