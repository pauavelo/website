---
title: "L'europe et la passerelle"
date: 2023-05-07T23:49:16+01:00
author: "Sébastien Lamy"
tags: 
  - passerelle
  - accessibilité
  - voie verte
  - véloroute V81
  - action
  - Bizanos
  - Mazères-Lezons
  - Europe
  - balade
---

Ce dimanche 7 mai, Pau à Vélo accompagnait la "Balade de l'Europe". À l'occasion de la Fête de l'Europe, [Pistes Solidaires - Europe Direct Pau Pays de l'Adour](https://www.europe-direct-ppa.fr/), en collaboration avec la Ville de Pau et l'association Pau à vélo, proposait deux balades, l'une à pied et l'autre à vélo, pour découvrir l'Europe au quotidien.

Les parcours des balades avaient vocation à nous faire percevoir comment l'Union européenne est présente dans notre vie de tous les jours, dans nos lieux de vie, ainsi que dans notre patrimoine historique et naturel.

<p><a href="https://www.openrunner.com/route-details/18343392"><img src="parcours.jpg" title="Ouvrir le parcours sur openrunner"></a></p>


Pour le circuit à vélo, nous sommes passés au stand de l'atelier vélo participatif et solidaire, les jardins de Saragosse, les pistes cyclables le long du parcours Fébus, le parlement de Navarre et le château de Pau, la véloroute v81, la passerelle de Franqueville, et le boulevard des Pyrénées. Autant d'éléments de notre décor local qui ont chacun bénéficié d'un financement européen à un moment de leur développement.

{{< gallery "photos" >}}

Arrivé à la paserelle de Franqueville, notre association a fait un happening pour rappeler l'inconcruité des marches qui mènent à cette passerelle côté Mazères, et la nécessité de changer les choses pour la rendre accessible aux PMR, poussettes, vélos cargos.

![](gilet-et-passerelle.jpg)

Déguisés en politique, scientifique, et ouvriers du BTP, quelques acteurs "Pau à Vélo" ont tenté de résoudre ce problème avec un système de corde et de force humaine.

{{< gallery "happening" >}}

Bien sûr ils ont du essuyer un échec, car la création d'une rampe d'accès est la seule réalisation fiable et envisageable pour régler le problème. [Notre proposition est publiée et nous l'avons fait connaître](/blog/2018/nous-avons-exige-une-passerelle-accessible-a-tous/), mais il semble qu'elle n'a pas trouvé son chemin jusqu'aux cartons de l'agglomération.

Pour l'heure, cette réalisation reste illégale, et il est très regrettable que les pouvoirs locaux soient parvenus à utiliser un fond européen pour réaliser un ouvrage qui n'est pas accessible à tous.

