---
title: "Voyage à Lourdes"
date: 2023-05-13T10:30:00+01:00
author: "Pierrick Hospital"
tags:
  - voyage
  - camping
  - Lourdes
  - forêt
---

Ce week-end du 13 et 14 mai 2023, notre assocation proposait un voyage jusqu'à Lourdes. Nous étions 10 inscrits, chacun avait fait révisé son vélo et préparé ses sacoches avec de quoi manger et dormir pour 2 jours.
La météo n’était pas très encourageante mais cela n’a pas entamé notre enthousiasme.

Nous avions réservé des lodges et des emplacements tentes au camping d’Arrouach à Lourdes.


Le samedi 13 mai après un dernier regard sur le grand prix, les 5 vélos électriques et les 5 vélos musculaires sont partis sans regret sur la V81, direction plein Est. Nous avons cheminé tranquillement (12 km/h de moyenne).

{{< gallery "photos/debut" >}}
 
Nous avons fait la pause pique-nique à Nay, puis juste après avoir déjeuner, nous avons profité de l’abri sous les arcades devant la Maison Carrée pour laisser passer une averse.

{{< gallery "photos/nay" >}}


Alors qu’un nouveau gros nuage menaçait, nous avons cherché et trouvé une grange pour nous mettre à l’abri.
L’agriculteur nous a permis de rester le temps de l’averse. Il est venu discuter et nous a présenté son travail et ses machines. C’était très sympa et intéressant.

{{< gallery "photos/grange" >}}

Une heure plus tard, nous nous sommes remis en chemin. Le rythme était tranquille, mais après St-Pé-de-Bigorre, le relief se fait plus escarpé, et la fatigue accumulée complique un peu la progression.
Et la pluie a repris. Moins violente, mais partie pour durer, ça ne servait à rien d’attendre.
Les plus rapides ont pris le temps d’attendre les plus lents.
Les plus lents ont repris des forces dans la forêt de Lourdes et ne se sont pas découragés.

{{< gallery "photos/st-pe" >}}

Nous sommes arrivés au camping complètement trempés.
Heureusement que nous étions (à peu près) bien équipés :
Pantalons anti-pluie, veste avec capuche, ponchos, sacs plastiques pour les pieds,… :D

{{< gallery "photos/camping" >}}

La soirée et la nuit au camping se sont très bien passés.
Nous avons pu nous reposer de cette journée sportive.
Il a pas mal plu mais nous étions tous bien protégés.
(Il faudra quand même que Delphine et Lionel s’achètent une vraie tente pour la prochaine fois!)

![](photos/tente-fragile.jpg)

Le lendemain matin, après le petit-déjeuner, nous sommes repartis… sous la pluie.

{{< gallery "photos/depart-lourdes" >}}

Heureusement, une fois les premiers kilomètres franchis, la pluie s’est calmée.
Le profil en faux plat descendant aidant, nous avons pu progresser à un bon rythme (13,8 km/h de moyenne).
Pour le pique-nique, nous avons pu faire une pause chez l’ami de Jany: Jean Serrien à St-Abit. Ça a également été un bon moment de partage :)

{{< gallery "photos/repas" >}}

Dans l’après-midi, la pluie nous a laissé tranquille jusqu’à Pau où nous sommes arrivés avant 16h, sous le soleil.

{{< gallery "photos/arrivee-pau" >}}

Les participants étaient tous ravis de leur week-end.
Tous sont prêts à repartir pour un prochain voyage, en espérant une meilleure météo.

