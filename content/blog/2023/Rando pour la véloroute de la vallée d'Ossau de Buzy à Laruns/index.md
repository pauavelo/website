---
title: "Rando pour la véloroute de la vallée d'Ossau de Buzy à Laruns"
date: 2023-09-09T10:22:08+01:00
author: "Michel Barrère et Sébastien Lamy"
tags:
  - véloroute vallée d'Ossau
  - Bielle
  - animations
---

Samedi 9 septembre, Dans le cadre de la fête des voies vertes, l’AF3V (que Pau à Vélo représente en tant que délégation départementale) s'est associée au CoCoTiers pour proposer une randonnée cycliste au départ de la gare de Buzy jusqu’à l’Espace naturel du Lac de Castet.

La rando vélo en vallée d’Ossau avait un triple objectif : 
1. pédaler en cœur hors circulation automobile
2. participer à un festival dédié à la protection de l’environnement (le [Pic Vert Festival](https://www.eteossalois.fr/pic-vert-festival/))
3. partager notre souhait pour la poursuite du projet de véloroute de Buzy à Laruns.


Deux convois de cyclistes ont été formés, avec des itinéraires et des vitesses différentes pour chacun.

{{< gallery "aller" >}}


Le convoi le plus rapide a pu se rendre devant la mairie de Bielle.

{{< gallery "mairie-bielle" >}}

A midi, tout le monde s'est retrouvé dans la zone de loisir du lac de Castet, pour pique niquer et profiter du Pic Vert Festival, avec notamment à 14h30 une conférence débat sur les mobilités douces. 

{{< gallery "conference" >}}

Vu son intérêt aussi bien pour les pratiquants que pour l’attractivité et l’économie de la vallée, le projet d’une voie verte en continu de Buzy à Laruns sur l’emprise de l’ancienne voie ferrée désaffectée, magnifique initiative de la Communauté de Communes de la vallée d’Ossau a été initialement approuvé à l’unanimité des communes traversées et a reçu les financements du département, de la région, de l’Europe, du Feder.

La première partie, déjà réalisée, d’une douzaine de kilomètres est très agréable, dans un beau cadre de montagne (avec très peu de dénivelé) et des aménagements de qualité (enrobé lisse, tables de pique-nique en pierre d’Arudy, informations, postes de gonflage et réparation vélos, traversées sécurisées).

![](/agenda/2023/rando-pour-la-veloroute-de-la-vallee-dossau/bitume.jpg)

Cette belle réalisation s’interrompt au rond-point Laspalettes, sur la commune de Bielle. La suite nécessite un aménagement non réalisé à ce jour.

![](/agenda/2023/rando-pour-la-veloroute-de-la-vallee-dossau/chemin.jpg)

Cette randonnée festive voulait remettre ce projet sur le devant de la scène en montrant l’appétit du public pour cet itinéraire, et pour sa finalisation sur les tronçons manquants.

La voie verte est un espace partagé. Les piétons, les cyclistes, les rollers, les coureurs, les personnes en fauteuil roulant, les familles avec poussette, les animaux et les engins agricoles devraient y trouver leur place en respectant l’autre.


{{< gallery "retour" >}}


![](julien-savary-02.jpg)
