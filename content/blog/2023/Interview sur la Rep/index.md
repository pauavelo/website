---
title: "Interview sur la Rep"
date: 2023-05-23T00:01:56+01:00
author: "Sébastien Lamy"
tags:
  - médias
  - rues scolaires
  - REV
---

Suite à l'annonce du plan vélo national, Marie Berthoumieu, du journal "La république des pyrénées", nous a invité pour une longue interview. Nous avons pu y exposer nos ambitions et nos vues sur le développement du vélo, à l'échelle locale ou nationale. REV(Réseau express vélo), quartiers apaisés, rue scolaire, communication, etc... Une pleine double page qui balaye large et que vous pouvez [retrouver sur le site de la république des pyrénées](https://www.larepubliquedespyrenees.fr/societe/associations/pau-a-velo-nous-ne-sommes-pas-des-khmers-verts-15270140.php)

Crédit photo: Marc Zirnheld @ La république des pyrénées
