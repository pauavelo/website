---
title: "Pot à Vélo"
date: 2023-06-09T18:54:55+01:00
author: "Sébastien Lamy"
tags:
  - animations
---

Ce vendredi 9 juin, notre association profitait des beaux jour pour proposer un "pot à vélo" chez Aquiu, rue Carnot. L'occasion de se retrouver et d'échanger entre convaincus de la mobilité active !

{{< gallery "photos" >}}
