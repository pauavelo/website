---
title: "Pot à vélo"
date: 2023-12-01T00:49:20+01:00
author: "Sébastien Lamy"
tags:
  - animations
---

Ce vendredi 1er décembre, les sympathisants des déplacements actifs étaient invités à prendre un pot au café Durango, au Hédas à Pau. L'occasion de papoter actualité et vélo, mais surtout d'aider à préparer les étiquettes qui invitaient à [notre soirée du 15 décembre](
/agenda/2023/deambulation-lumineuse/). 

![](preparation-etiquettes.jpg)
