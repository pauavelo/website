---
title: "Les roues de l'avenir"
date: 2023-12-15T20:47:41+01:00
author: "Sébastien Lamy"
tags:
  - ciné
  - animations
---

Après la [déambulation lumineuse](/blog/2023/deambulation-lumineuse/), nous avons terminé la soirée par la projection du film “Les roues de l’avenir”, à 20h, au cinéma le Méliès, en présence de Baptiste Lemaitre.

![](debat.jpg)

Ce film aborde la place du vélo dans notre société et questionne les différentes manières d’en faire un acte majeur de la transition environnementale et sociétale. Une réflexion pour faire avancer le débat et imaginer l’avenir du vélo quotidien. La projection a eu lieu en présence de Baptiste Lemaitre, porte parole du film, qui a pu répondre aux questions en fin de séance.

![](affiche.jpg)
