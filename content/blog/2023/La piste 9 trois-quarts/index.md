---
title: "La piste 9 3/4"
date: 2023-10-21T00:12:26+01:00
author: "Sébastien Lamy"
tags:
  - continuité cyclable
  - action
  - aménagements
  - gare
---

À proximité de la gare, on peut déjà observer une piste cyclable presque terminée. Cette piste part d'une bonne intention, mais elle s'arrête brutalement à quelques dizaines de mètres de la gare, semblant acter que la discontinuité des itinéraires cyclables est une règle d'un passé qui reste immuable à Pau, pour aujourd'hui comme pour demain.

![](piste-9-tois-quart.jpg)


Mais ce samedi après-midi, des cyclistes dotés de pouvoirs magiques nous ont révélés le vrai sens de cette piste: il s'agit de l'entrée de la piste 9 3/4, que eux seuls peuvent emprunter pour accéder au quai magique de la gare (le quai dont part le train pour l'école des sorciers). 


![](copie-de-baguette-harry-950-x-1350-px.jpg)

{{< gallery "photos" >}}

Pour les autres (les cyclistes moldus sans magie), il s'agit juste d'un obstacle infranchissable. Les fans de Harry Potter sont très familiers de ce type d'aménagement.


![](img-20231021-wa0102.jpg)

La piste  9 3/4 s'arrête net et oblige les usagers du vélo démunis de pouvoirs magiques à  s'insérer à leur risques et périls dans la voie automobile sans aucune sécurisation.

Mais pour tous les "moldus" à vélo, **la continuité et la sécurisation des aménagements cyclables est indispensable** pour que la part modale du vélo augmente, que l'argent dépensé dans les nouveaux aménagements ne soit pas inutile. La quantité de voies cyclable ne suffit pas, la qualité et la continuité sont primordiales.


Vous pouvez consulter [le courrier adressé préalablement par Pau à Vélo à la mairie de Pau](lettre-accessibilite-gare.pdf), au sujet de cette nouvelle piste cyclable aux abords de la gare.


Cette action a fait l'objet d'un [article dans la république des pyrénées](https://www.larepubliquedespyrenees.fr/economie/transports/pau-a-velo-peste-avec-le-sourire-de-la-nouvelle-bande-cyclable-de-la-gare-17161869.php)

