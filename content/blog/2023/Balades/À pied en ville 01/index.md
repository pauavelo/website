---
title: "À pied en ville #1"
date: 2023-02-19T10:00:00+01:00
author: "Véronique caignon"
tags:
  - balade à pied
---

Le dimanche 19 janvier, sous la clémence du ciel et des températures, une douzaine de participants (cyclistes habituellement ou pas) a initié la première promenade à pied de Pau à vélo. En effet, pour répondre à des enjeux et des besoins environnementaux, la marche urbaine doit devenir, à l’image de la pratique du vélo au quotidien, une mobilité à défendre auprès des décideurs. Aussi, à l’issu de chaque promenade urbaine, les points positifs et négatifs seront échangés entre les différents participants.

Arrivés à vélo, en transport en commun ou à pied, les participants de Pau à Vélo ont pu profiter des différents aménagements piétons mis à leur disposition par la ville de Pau et Jurançon pour profiter d’une balade urbaine.

<p><a href="https://www.openrunner.com/route-details/16178274"><img src="parcours.jpg" title="Ouvrir le parcours sur openrunner"></a></p>


Les points qui ont été relevés au cours de cette première balade :
* Le groupe a apprécié certains aménagements comme la voie verte devant les jardins Marsan. Endroit si agréable que nous y avons partagé un moment convivial et gastronomique. De plus, une partie du groupe a pu également profiter du carnaval au Hédas.
* La traversée des ponts d’Espagne et 14 juillet n’a pas été aimée. Les piétons, arrivant de la voie verte le long du gave depuis Jurançon, ne savent pas où traverser sur le cours 14 juillet (pas de passage piéton, sauf un éloigné et dont l’accès est bloqué par une grille de chantier, aucun itinéraire bis de prévu). Il en est de même au niveau de la place de la monnaie. Heureusement que nous réalisons nos promenade le dimanche !!! 
* En cours d’aménagement, le pont du 14 juillet ne présage pas une déambulation agréable malgré une belle vue sur les différents bras du gave, grâce au nouveau garde-corps ajouré. 

