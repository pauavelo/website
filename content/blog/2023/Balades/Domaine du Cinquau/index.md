---
title: "Domaine du Cinquau"
date: 2023-07-02T15:57:01+01:00
author: "Pierrick Hospital"
tags:
  - balade
  - vignes
  - véloroute V81
---

Pour ce premier week-end du mois de juillet, la météo était plutôt chaude !

C’était l’occasion de vous proposer une balade un peu plus longue que d’habitude.
Nous sommes allés jusqu’au domaine du Cinquau à Artiguelouve.

<p><a href="https://www.openrunner.com/route-details/18343742"><img src="parcours.jpg" title="Ouvrir le parcours sur openrunner"></a></p>

L’aller faisait 15km à peu près, sur la véloroute V81 globalement, en descendant tranquillement le long du gave.
Il n’y avait pas de dénivelé, et nous étions éloignés de la circulation. A quelques minutes de l'arrivée, nous avons du essuyer une grosse averse qui s'est arrêtée avant notre arrivée. Heureusement, nous avons pu mettre nos affaires à sécher.

Sur place, nous avons été accueilli par Pierre qui nous a présenté le domaine avec des anecdotes historiques personnelles, et nous a fait faire un petit tour du propriétaire. Nous avons également pu visiter les champs de vignes à pied.
Il était également possible, pour ceux qui voulaient, de déguster certains vins du domaine, moyennant une participation financière.

Nous avons ensuite pris le temps de manger sur place le pique-nique sorti du sac.

{{< gallery "photos" >}}

Après manger, nous sommes revenus par le même chemin.
