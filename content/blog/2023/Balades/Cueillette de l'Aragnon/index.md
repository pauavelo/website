---
title: "Cueillette de l'Aragnon"
date: 2023-09-03T17:07:32+01:00
author: "Sébastien Lamy"
tags:
  - balade
  - cueillette
---

<p><a href="https://www.openrunner.com/route-details/18344073"><img src="parcours.jpg" title="Ouvrir le parcours sur openrunner"></a></p>

Pour cette balade à la cueillette de l'Aragnon a été très familiale, plusieurs parents ayant choisi d'y emmener leurs enfants. Nous avons emprunté des chemins et routes peu fréquentés. Nous avons passé une petite heure à la cueillette. Le temps était nuageux avec des éclaircies, et pas de pluie.

{{< gallery "photos" >}}
