---
title: "Ferme Pau de vaches et Écopernic"
date: 2023-12-03T18:39:11+01:00
author: "Pierrick Hospital"
tags:
  - balade
  - ferme
  - écolieu
  - forêt de Bastard
---

Pour le premier dimanche de décembre, il faisait frisquet, mais le soleil était présent :sunny:
Nous vous proposions d’aller jusqu’à [la ferme Pau de vaches](https://www.facebook.com/Paudevaches/photos?locale=fr_FR) au nord de Pau en passant par la forêt de Bastard.

{{< gallery "photos" >}}

Christophe nous a fait une petite présentation et nous avons pu acheter leurs bons produits. 
Ils font du fromage et du saucisson du tonnerre ! (et aussi des yaourts) :stuck_out_tongue:

![](fromage-saucisson.jpg)

Au retour, après avoir traversé l’autoroute, nous avons fait un passage par [Écopernic](https://www.ecopernic.fr/).

![](ecopernic.jpg)

Françoise et les autres résidents du lieu nous ont organisé une petite visite de cet écolieu et nous ont parlé de maison bioclimatique.


Le parcours en boucle était très court: à peine 10km et pratiquement pas de dénivelé.

<p><a href="https://www.openrunner.com/route-details/18344781"><img src="parcours.jpg" title="Ouvrir le parcours sur openrunner"></a></p>
