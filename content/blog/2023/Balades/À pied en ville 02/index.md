---
title: "À pied en ville #2"
date: 2023-03-19T10:00:00+02:00
author: "Sébastien Lamy"
tags:
  - balade à pied
---



Notre deuxième balade à pied nous a mené vers le parc Lawrence et ses magnifiques cèdres du Liban. Nos parapluies n'ont pas été inutiles pour faire face à de petites averses.

<p><a href="https://www.openrunner.com/route-details/16311296"><img src="parcours.jpg" title="Ouvrir le parcours sur openrunner"></a></p>


En chemin, un passage par la place de la libération, puis le quartier Foirail-Montpensier et les abords de la nouvelle salle de spectacle en travaux. Au retour, dans la très désagréable rue Carnot, nous découvrons le nouveau passsage Carnot, petit havre de paix piéton. L'aventure se termine autour de gateaux et de tisanes sur la terrasse des halles.

![](img-20230319-111334927.jpg)

