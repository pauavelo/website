---
title: "Chasse aux papillons"
date: 2023-06-24T16:24:33+01:00
author: "Kevin Le Falher"
tags:
  - CEN
  - balade
  - papillons
  - côteaux
  - Laroin
  - véloroute V81
---

Le Conservatoire des Espaces Naturels (CEN) organisait une sortie naturaliste ce samedi 24 juin de 9h30 à 12h30 à la découverte de la biodiversité des coteaux de Laroin. Nous sommes partis en vélo depuis l'antenne de Conservatoire d'Espaces Naturels Nouvelle-Aquitaine, association pour laquelle je travaille, en direction des coteaux de Las Hies où sont notamment présents quelques espèces de papillons rares et protégées. Nous y avons fait un arrêt pour faire le tour et découvrir les espèces de plantes, papillons, oiseaux, etc.

{{< gallery "photos" >}}

L'itinéraire faisait 12 km aller/retour et était relativement plat. 

<p><a href="https://www.openrunner.com/route-details/18343850"><img src="parcours.jpg" title="Ouvrir le parcours sur openrunner"></a></p>


Des filets à papillons et clés de détermination "papillons de jour" ont été fournis au public.

Notes de Pau à Vélo : 
C'était vraiment une super journée, et nous sommes heureux d'avoir un partenaire qui apporte autant de plus-value à nos sorties. Nous espérons que cette relation pourra porter d'autres fruits à l'avenir, merci encore à Kevin pour son excellente initiative !
