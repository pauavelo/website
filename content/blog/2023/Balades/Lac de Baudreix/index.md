---
title: "Pique nique au lac de Baudreix"
date: 2023-08-06T12:13:03+02:00
author: "Pierrick Hospital"
tags:
  - balade
  - forêt de Bastard
  - lac de Beaudreix
  - véloroute V81
---

Pour ce premier dimanche du mois d'août, la météo a été globalement favorable.

Nous en avons profité pour faire une balade un peu plus longue que d’habitude.
Nous sommes allés jusqu'au lac de Baudreix pour pique-niquer.

Pour changer de l'itinéraire habituel, nous avons fait l'aller par la rive droite du gave.
Quelques passages sur des routes : un bout de l'avenue Albert 1er à Bizanos, puis la rue Matachot à Aressy.
Mais le plus difficile finalement, c'était les petits chemins dans la forêt proches du gave.
C'était très bucolique, mais assez inconfortable.

Pour le retour, nous sommes restés sur la V81, qui est globalement éloignée de la circulation, sauf pour le tronçon qui monte vers Baliros.

Le parcours (en boucle donc) faisait un peu moins de 40km.
Ceux qui voulaient rallonger ont pu aller jusqu'à Nay (+4km).

La seule côte a été celle de Baliros.

Voici le parcours :
<p><a href="https://www.openrunner.com/route-details/17338195" target="blank"><img src="parcours.jpg" title="Ouvrir le parcours sur openrunner"></a></p>

Et quelques photos (merci à Françoise Poinsot):

{{< gallery "photos" >}}
