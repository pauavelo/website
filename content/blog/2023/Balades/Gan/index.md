---
title: "Gan"
date: 2023-10-01T17:34:06+01:00
author: "Pierrick Hospital"
tags:
  - balade
  - Gan
  - véloroute vallée d'Ossau
  - lac
---

Ce dimanche 1er octobre, nous vous proposions une balade dans la vallée du Néez jusqu'à Gan. La météo était excellente !
L'itinéraire empruntait ce qui sera (peut-être un jour) le début de la véloroute de la vallée d'Ossau.

<p><a href="https://www.openrunner.com/route-details/17721627"><img src="parcours.jpg" title="Ouvrir le parcours sur openrunner"></a></p>

À Gan, nous sommes allés voir le lac de la Téulère (la tuilerie). Nous avons aussi vu la place carrée de la Bastide, et la porte de la prison, au centre ville.

{{< gallery "photos" >}}
