---
title: "À pied en ville #3"
date: 2023-06-18T22:19:28+01:00
author: "Véronique Caignon et Sébastien Lamy"
tags:
  - balade à pied
  - parc
  - square
  - jardin
---

Ce dimanche 18 juin, armé de nos parapluie, nous avons marché tranquillement à travers les rues de notre belle ville de Pau. 

<p><a href="https://www.openrunner.com/route-details/18346136"><img src="parcours.jpg" title="Ouvrir le parcours sur openrunner"></a></p>


Depuis le Boulevard des Pyrénées, nous sommes partis vers la gare fraîchement sortie de ses travaux, puis vers le stade nautique et ses parcs (square Georges Besson et jardin de Kofu). 

Nous avons pris notre pique-nique dans un temps maussade, au très agréable square Georges Besson.

{{< gallery "photos" >}}

