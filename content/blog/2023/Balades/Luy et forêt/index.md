---
title: "Luy et forêt"
date: 2023-06-04T15:44:36+01:00
author: "Pierrick Hospital"
tags:
  - balade
  - forêt de Bastard
  - luy
---


<p><a href="https://www.openrunner.com/route-details/18343509"><img src="parcours.jpg" title="Ouvrir le parcours sur openrunner"></a></p>

Après un mois de mai chargé avec plusieurs balades à thème (découverte des friches, balade de l'Europe,…),
nous sommes repartis sur le rythme classique des balades mensuelles chaque 1er dimanche du mois.

Pour ce mois de juin, nous vous proposions une petite balade à l'extérieur de la ville.
Nous avons été au nord faire une boucle en passant par la forêt de Bastard et longer le Luy du Béarn. Le départ était au crayon du Leclerc.

{{< gallery "photos" >}}

En fin de parcours, nous avons eu à faire à un dérailleur cassé, ce n'était pas une mince affaire !

