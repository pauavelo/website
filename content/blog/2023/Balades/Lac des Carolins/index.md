---
title: "Lac des Carolins"
date: 2023-11-05T18:19:09+01:00
author: "Sébastien Lamy"
tags:
  - balade
  - lac
  - forêt de Bastard
---

Comme on dit : il n'y a pas de mauvais temps, il n'y a que de mauvais équipements. Ce dimanche malgré un avis météo défavorable, le temps était parfaitement supportable et nous sommes allés, bien équipés, jusqu'au lac des Carolins.

<p><a href="https://www.openrunner.com/route-details/18344635"><img src="parcours.jpg" title="Ouvrir le parcours sur openrunner"></a></p>

Le parcours en boucle passait par la forêt de Bastard et par des petits chemins le long de l'Ousse des Bois au retour.
Au total 16km, pratiquement pas de dénivelé.

Pierrick était sur un vélo de secours, prêté par l'Atelier Vélo (en attendant une réparation sur son vélo habituel). Mais la chaîne du vélo de secours n'était pas très solide et l'a lâché sur le chemin du retour. Merci à Jean-Philippe pour son cable antivol, et à Josette pour avoir tiré notre valeureux guide.

{{< gallery "photos" >}}
