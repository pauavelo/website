---
title: "Pau à Vélo en roue libre"
date: 2023-05-06T22:31:11+01:00
author: "Marie Daviet"
tags:
  - animations
  - concert
  - vélorution
  - médias
  - conte
---

Le 06 mai, Pau à Vélo fêtait ses vingt ans à la Forge Moderne, lors de leur événement "La Forge en roue libre".

La fête a commencé par la «vélorution du petit prix altermobile». Une cinquantaine de cyclistes se sont élancé-es sur une route parfaitement lisse pour faire non pas un mais deux tours du circuit du grand prix de Pau et rappeler que le vélo, c'est un moyen de déplacement moins bruyant et moins polluant que certains véhicules motorisés.


![](velorution.jpg)
{{< gallery "petit-prix" >}}

Cette vélorution s'est finie à la Forge moderne pour participer à la "Forge en roue libre". Le programme de cette soirée était varié.

Une course de lenteur a été proposée par Pau à Vélo, mais aussi un quizz pour tester ses connaissances sur le vélo et les modes de transport.

![](course-lenteur.jpg)
{{< gallery "course-lenteur" >}}

![](quizz.jpg)

Pour les adeptes de culture, la Compagnie Mon P'tit Guidon proposait son conte musical _La Ville extraordinaire_ avant qu'Aquiu project anime la fin de soirée par un concert sur des airs venus d'ici et d'ailleurs.

![](spectacle.jpg)
{{< gallery "conte-musical" >}}

![](concert.jpg)
{{< gallery "concert" >}}

Les amoureux et amoureuses de vélo ont (re)découvert des modèles de vélo moins classiques, du vélo cargo au vélo couché, en passant par des vélos adaptés.

![](velos-atypiques.jpg)
{{< gallery "velo-atypiques" >}}

Et pendant que certains et certaines buvaient diverses boissons, deux expositions mettant les cyclistes et le vélo à l'honneur étaient affichées à l'intérieur et à l'extérieur et accessibles tout au long de ces différents moments. Par exemple, les photos des Chats noirs donnaient à voir des aventures cyclistes dans de beaux paysages. 


![](chat-noir.jpg)
{{< gallery "expo-photo" >}}

Une belle soirée pour mettre à l'honneur le vélo, les cyclistes et la joie de pédaler !


![](web-sign-9205.jpg)

Les évènements du week end dans leur ensemble ont fait l'objet d'un [long article dans sud-ouest](https://www.sudouest.fr/economie/transports/on-va-beaucoup-plus-vite-a-velo-qu-en-voiture-pau-a-velo-20-ans-et-toujours-au-taquet-15058561.php)
La vélorution du petit prix altermobile a fait l'objet d'[un article dans la république des pyrénées](https://www.larepubliquedespyrenees.fr/economie/transports/pau-a-velo-organise-son-grand-prix-alternatif-15059752.php).
