---
title: "Journée de l'environnement à Franqueville"
date: 2023-09-17T10:24:16+01:00
author: "Sébastien Lamy"
tags:
  - animations
  - balade
  - Franqueville
  - papillons
  - libellules
---

Ce dimanche 17 septembre c'était la fête de l'environnement au château de Franqueville, à Bizanos. Avec le Conservatoire des Espaces Naturel (CEN), nous vous proposions une balade à vélo le matin, accompagnés de connaisseurs passionnés, pour aller observer libellules et papillons.

<p><a href="https://www.openrunner.com/route-details/18344183"><img src="parcours.jpg" title="Ouvrir le parcours sur openrunner"></a></p>

{{< gallery "photos" >}}

Toute la journée, nous avons également tenu un stand parmi les nombreuses associations présente.

![](affiche.jpg)

L'ambiance était bonne, le soleil était au rendez-vous, et les conférences semblaient intéressantes. Cependant, nous avons vraiment déploré le fait que **le parc du château ait été transformé en parking géant pour accueillir les visiteurs :scream: !**  Quel paradoxe pour une journée de l'environnement ! Attirer du monde, c'est un bon objectif, mais que doit-on sacrifier à la culture du "tout voiture" lors d'une telle journée ?
