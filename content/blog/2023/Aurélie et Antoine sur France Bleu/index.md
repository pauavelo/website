---
title: "Aurélie et Antoine sur France Bleu"
date: 2023-04-20T10:16:02+01:00
author: "Sébastien Lamy"
tags:
  - médias
  - conseils
---

Ce jeudi 20 Avril, Aurélie et Antoine ont été invité à représenter notre asso dans l'émission "les experts" animée par Anna Lucianni sur France Bleu.

Thème : Se (re)mettre au vélo avec les beaux jours !

Au menu: les bonnes raisons de se mettre au vélo, quelques idées des aides disponibles, quelle vélo choisir, comment rouler en sécurité ?

Retrouvez cette émission dans [les archives de France Bleu](https://www.francebleu.fr/archives/emissions/les-experts-de-france-bleu-bearn/bearn/2023?pageCursor=MTY4)(*suivre le lien et dérouler la page jusqu'à trouver l'émission du 20 avril*). Vous pouvez aussi la [télécharger ici](20-04-2023-les-experts.mp3)

Merci à France Bleu de nous avoir invité, et bravo à Antoine et Aurélie pour leur intervention !
