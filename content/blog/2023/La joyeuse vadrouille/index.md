---
title: "La joyeuse vadrouille"
date: 2023-10-15T11:51:35+01:00
author: "Sébastien Lamy"
tags:
  - animations
  - ciné
  - enfants
---

Ce dimanche 15 octobre, Pau à Vélo organisait la projection de "La Joyeuse Vadrouille" au Méliès. Ce film est un journal de bord d'une famille en voyage à vélo au long cours, avec un tout petit enfant.

![](visuel.jpg)

Pour se mettre dans le sujet, notre association proposait une balade familiale, histoire de partir ensemble du boulevard des pyrénées et se rendre au cinéma en faisant quelques détours !

{{< gallery "balade" >}}

90 personnes étaient présentes à la projection. Timothée Buisson, le réalisateur bordelais, a hélas annulé tardivement sa venue sur Pau, suite à des problèmes de train. Pour palier à son abscence, après la projection du film, différents adhérents de notre association ont témoigné de leur expérience de voyage en famille avec des enfants.

![](img-20231015-wa0004.jpg)

L'évènement s'est clôt par un moment convivial offert par l'association au café du Méliès.

{{< gallery "film" >}}

