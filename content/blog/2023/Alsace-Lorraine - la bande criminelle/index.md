---
title: "Alsace-Lorraine : la bande criminelle"
date: 2023-10-01T23:20:45+01:00
author: "Sébastien Lamy"
tags:
  - Alsace-Lorraine
  - action
  - médias
  - Carnot
---

Ce dimanche premier octobre, des membres de l'association Pau à Vélo ont pris l'initiative de peindre des bandes cyclables adaptées au boulevard Alsace-Lorraine et à ses nombreux stationnements débordants. Ils ont été interrompus par la police municipale...

{{< gallery "alsace-lorraine" >}}

Certains ne se sont pas découragés : pas loin, la rue Carnot était fermée à la circulation pour cause de travaux. Ils en ont donc profité pour rafraichir et augmenter la signalétique au sol. Il faut dire que dans cette rue, [les règles de circulation sont compliquées !](/blog/2023/rue-carnot-le-feuilleton/)

{{< gallery "carnot" >}}

Sur le boulevard Asace-Lorraine, l'opération visait à mettre en lumière la dangerosité de cet axe pour le vélo au quotidien : Sorties de stationnement impromptues, bande à nouveau effacée, véhicules de gros gabarit qui empiètent largement sur la bande (et trottoir!), gravillons et résidus, revêtement détérioré... Le baromètre des villes cyclables pointe de nombreux points noirs sur cet axe, et il a été le théâtre de plusieurs accidents mortels au cours des années passées.

L'association Pau à Vélo réclame de réorganiser l'axe Alsace Lorraine depuis quelques années. Le stationnement y prend une place qui doit être sérieusement remise en question pour sécuriser les mobilités actives. **La configuration actuelle, avec la bande cyclable à la merci de tous les problèmes liés au stationnement et occupation abusive, est d'une dangerosité criminelle.**

Dans un contexte de rentrée d'insécurité routière marquée (les 3 dernières semaines) : un enfant de 3 ans décédé proche de la sortie de l'école Marancy, un cycliste de 38 ans tué sur le rond point Gabriel Delaunay, une cycliste renversée puis écrasée sous la voiture à Jurançon place du Junqué, une autre cycliste percutée par un poids lourds dépassant trop près sur la rocade de Jurançon, un cycliste heurté par un véhicule municipal lui faisant une queue de poisson sur le boulevard Alsace Lorraine, sans parler de tous les autres accidents qui ne parviennent pas à nos oreilles, les cyclistes se retrouvent dans une situation anxiogène qui appelle des réponses urgentes, fortes et ambitieuses. La police municipale a rappelé aux peintres improvisés que la mairie est la seule légitime pour le faire. Qu'attend-elle pour agir ?

Articles de presse sur notre action :
* [La république des pyrénées pour la partie Alsace-Lorraine](https://www.larepubliquedespyrenees.fr/pyrenees-atlantiques/pau/une-operation-de-pau-a-velo-pour-denoncer-la-dangerosite-du-boulevard-alsace-lorraine-16895307.php)
* [La république des pyrénées pour la partie Carnot](https://www.larepubliquedespyrenees.fr/economie/transports/clin-d-oeil-a-pau-la-ville-et-l-association-pau-a-velo-travailleraient-de-concert-16911405.php)
