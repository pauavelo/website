---
title: "Café climat"
date: 2023-04-06T18:30:00+02:00
author: "Marie Daviet"
tags:
  - animations
  - conférence
---

Une ville sans voiture est-elle possible (et désirée) ?

Pour son café-climat du mois d'avril, [Ecocène](https://www.ecocene.fr/) a décidé de nous interroger sur la place de la voiture en ville.

Alors que de nombreuses personnes étaient présentes et s'étaient confortablement installées sur les chaises au rez-de-chaussée de l'ancien local d'Aquiu, Pau à vélo a invité une grande partie d'entre elles à se déplacer et se masser dans les quelques espaces restants avant de se lancer dans un voyage en voiture emmenant les spectateurs et spectatrices à voir la pollution de l'air, le bruit, la place prise par la voiture, l'effet des conditions climatiques sur le trajet ou encore le temps passé en embouteillage. 

Après cette démonstration des nombreux effets négatifs de l'automobile en ville, Laétitia Lanardoune, cheffe de projets mobilités durables à l'agglomération paloise, a abordé les enjeux associés à la voiture dans l'agglomération et notamment la tension entre zone piétonne et heures de livraison ou encore celles liées aux transports en commun, allant de leur itinéraire à leur gratuité.

De son côté, Ecocène a rappelé quelques éléments-clés pour nourrir le débat : selon les chiffres de l'ADEME, en France, la moitié des trajets réalisés en France en voiture font moins de 5 kms. Pourtant, le transport est la première source d'émissions de gaz à effet de serre et la voiture représente plus de 50% de ces émissions. Avant de clotûrer ce café-climat, un espace d'expression libre sur papier a été proposé pour répondre à la question de ce café et identifier les freins qui empêchent de se passer de voitures. 

Et enfin, pour continuer la discussion et réfléchir à si une ville sans voiture est désirable mais aussi comment l'atteindre, les médiathèques de l'agglomération de Pau ont proposé une sélection d'ouvrages qui inclut _Pour des villes à échelle humaine_ de Jan Gehl, _Reconquérir les rues : exemple à travers le monde et pises d'action pour des villes où l'on aimerait habiter_ de Nicolas Soulier ou encore un ouvrage du Ministère de l'écologie, de l'énergie, du développement durable et de l'aménagement du territoire, _Ville et voiture_ . L'ensemble de la bibliographie est à retrouver dans les médiathèques 

Et pour un autre regard sur ce café-climat, rien ne vaut les mots de la République des Pyrénées (édition du 8 avril 2023).
<div class="gallery">
{{< image src="images/article-la-rep.jpg" caption="" width="543" >}}
</div>
