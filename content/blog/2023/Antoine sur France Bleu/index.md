---
title: "Antoine sur France Bleu"
date: 2023-09-18T17:12:08+01:00
author: "Sébastien Lamy"
tags:
  - médias
  - sécurité
  - accident
  - rond-point Gabriel Delaunay
  - rond-point
---

Ce mercredi 18 septembre 2023, Antoine était l'invité de la rédaction au 6-9 sur France Bleu, interviewé par Mathias Kern pour représenter notre association. 

**Sujets abordés** : l'accident d'Evgheny sur le rond point de Total, est-ce si dangereux de faire du vélo à Pau ? Les autocollants "pistes noires" ? Les priorités de Pau à Vélo pour l'aménagement ? Les améliorations récentes ? Les situations difficiles ?
Vous pouvez retrouver l'interview sur [la page dédié du site de France Bleu](https://www.francebleu.fr/emissions/l-invite-de-la-redaction-de-france-bleu-bearn-bigorre/la-securite-des-itineraires-est-primordiale-dit-antoine-lefeuvre-de-l-association-pau-a-velo-4912512), ou bien [la télécharger ici](2023-09-18-interview-antoine-france-bleu.mp3)

Bravo à Antoine pour son intervention, et merci à France Bleu de nous avoir invité !
_____
Photo de l'article : Radio France, issue du site francebleu.fr

