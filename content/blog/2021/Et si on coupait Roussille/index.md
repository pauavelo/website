---
title: "Et si on coupait Roussille ?"
date: 2021-09-01T16:41:55+02:00
author: "Sébastien Lamy"
tags:
  - véloroute V81
  - Rousille
---

Suite à l'annonce en janvier de travaux prévus sur l'avenue Amédée Roussille, nous avons entammé en juin 2021 une réflexion sur cette rue qui se trouve sur un itinéraire cyclable important : la v81.

Le problème de cette rue est qu'elle subit un trafic motorisé relativement important, constitué essentiellement de véhicules qui ne passent par là que pour couper au plus court. A vélo ou à pied, ce phénomène créé des conditions de circulation plus difficiles et plus dangereuses. Au final c'est une forme de rupture de la continuité de la véloroute.

Le 6 août, alors que les travaux sur la rue Amédée Roussille sont presque finis, nous avons transmis à nos interlocuteurs de la mairie une proposition de modification du plan de circulation qui permettrait d'apaiser cette rue. Cette proposition était inclue dans une vision plus large qui concernait tout le quartier, et elle était basée sur des constats et des mesures que nous avions documentés, vous pouvez la consulter ici.

<div class="pure-g trombi">
{{< dossier id="plan-circulation-roussille" title="Proposition de plan de circulation" >}}
</div>

Sommaire:
* État des lieux
* Proposition globale pour l'aménagement du quartier
* Proposition pour la rue Amédée Roussille
* V81 et rue Amédée Roussille

Le 31 août, nous avons évoqué cette proposition lors de notre réunion avec la mairie. Nos interlocuteurs se sont montrés intéressés par la proposition, mais voyaient de nombreuses étapes nécessaires avant sa réalisation :
* Simulation logicielle des impacts sur le trafic.
* Construction d'une réflexion plus globale incluant les modifications à venir sue le 14 juillet et la place de la Monnaie.
* Concertation avec les habitants.

La mairie nous annonce donc qu'un tel projet ne peut être envisagé qu'à long terme.
