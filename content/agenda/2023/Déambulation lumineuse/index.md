---
title: "Déambulation lumineuse"
publishdate: 2023-12-09T01:27:04+01:00
author: "Florence Nunès"
date: 2023-12-15
lieu: "Place de la reine Marguerite"
horaire: 17h30
tags:
---

Le 15 décembre, pour commencer une soirée festive, Pau à Vélo vous invite à une déambulation lumineuse. 
<!--more-->

Il s'agira de faire briller feux de vélos, guirlandes  et lanternes en déambulant en musique dans les rues du centre ville de Pau. L'ambiance devrait être joyeuse ! Le rythme et l'encadrement permettront à tout le monde de rouler en sécurité et sans se fatiguer excessivement.

Rendez vous à 17h30 place de la reine Marguerite, pour un départ à 18h. **Dès 17h30, un spectacle de jonglerie lumineuses sera proposée sur place**. Soyez à l'heure!

Pour ceux qui veulent participer à l'évènement mais ne peuvent pas pédaler, les triporteurs du gave vous proposent de vous transporter gratuitement ! Voir le message de Jean-Michel en bas de cet article.

Nous proposerons à ceux qui le souhaitent des lampions à arborer le temps de la déambulation.

![](lampion.jpg)

Aussi, si vous n'avez pas de feu pour votre vélo, c'est l'occasion d'en acheter à prix réduit ! Notre association vous proposera un modèle basique, rechargeable par usb, qui permet d'être bien vu (pas de voir dans le noir). 5 euros l'unité, 10 euros la paire avant/arrière, prévoyez de la monnaie.

![](eclairages.jpg)


La déambulation devrait être finie à 19h. A 20h, on se retrouve après manger pour aller voir [le film "Les roues de l'avenir" au Méliès](/agenda/2023/les-roues-de-lavenir/) !

> 
> Le Triporteur du Gave est à la disposition de personnes qui souhaiteraient participer à la déambulation lumineuse mais qui ne peuvent pas pédaler ce soir là, quelle qu’en soit la raison.
>
> Cela peut-être l’occasion, par exemple, d’inviter votre papi ou votre mamie,
un proche handicapé ou en petite forme, à participer !
>
> ![](triporteur.jpg)
>
> Nous pouvons embarquer 2 adultes à bord de nos vélos-canapés (à concurrence de 160 kg) et les prendre en charge au lieu de départ ou un autre endroit en
ville si cela est plus pratique. Le Triporteur est équipé d’une bonne couverture pour les jambes.
>
> Si vous êtes intéressé-e, merci de m’appeler pour les modalités pratiques. (c’est bien évident gratuit ). 
>
> Amicalement,  
Jean-Michel GARNERONE pour l’Association  
Lauréat du 1er Budget Participatif du Conseil Départemental 64  
Avec le soutien de La Mairie de Lescar  
/triporteurdugave  
06 20 39 86 65

