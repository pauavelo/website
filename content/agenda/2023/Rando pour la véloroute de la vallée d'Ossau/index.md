---
title: "Rando pour la véloroute de la vallée d'Ossau de Buzy à Laruns"
publishdate: 2023-08-15T11:30:37+02:00
author: "Pierrick Hospital"
date: 2023-09-09
lieu: "Gare SNCF de Pau"
horaire: 08h45
tags:
  - balade
  - véloroute
  - AF3V
  - Ossau
---

Dans le cadre de la fête des voies vertes, l'AF3V (que Pau à Vélo représente en tant que délégation départementale) s'associe au CoCoTiers une randonnée cycliste au départ de la gare de Buzy jusqu'à l'Espace naturel du Lac de Castet. Après un pique-nique partagé, nous profiterons du Pic Vert Festival avec notamment une conférence débat autour des mobilités douces.

<!--more-->

 La rando vélo du samedi 9 septembre en vallée d’Ossau a un triple objectif : il s’agit non seulement de pédaler en cœur hors circulation automobile mais aussi de participer à un festival dédié à la protection de l’environnement et de **partager notre souhait pour la poursuite du projet de véloroute de Buzy à Laruns**.

### Programme de la journée du samedi 9 septembre :
* **Rendez-vous à la gare SNCF de Pau à 8h45** pour les palois, départ du train à 9h16.
Cliquez sur le bouton ci-dessous pour réserver un billet AR « tribu » et pour le transport de vos vélos sur les remorques porte-kayaks prêtées par le PCKCU.  *Tarif: adulte: 6€ - enfant: 3€*. Attention, le transport des vélos est effectué sur des remorques qui pourrait légèrement rayer la peinture.

<iframe id="haWidget" allowtransparency="true" src="https://www.helloasso.com/associations/pau-a-velo/evenements/billets-de-train-pau-buzy-ar/widget-bouton" style="width: 100%; height: 70px; border: none;"></iframe>

* Arrivée à la **gare de Buzy à 9h37**. C’est là qu’est le véritable rendez-vous. En attendant l’arrivée des remorques de vélos, nous pourrons étudier les panneaux sur la géologie et l’histoire de la vallée et apprécier la qualité des équipements proposés. Nous y serons rejoints d’une part par ceux arrivant par leurs propres moyens et d’autre part par les cyclistes ossalois, notamment ceux invités par l’asso tiers-lieu le CocoTiers et le collectif Vallée Partagée. Deux convois différents se formeront alors qui se retrouveront tous au lac de Castet vers 12h
  1. Balade famille au rythme accessible à tous jusqu'au lac de Castet (23km aller-retour sur la journée). Ce convoi marquera une pause pour voir le dolmen de Buzy. Pour ceux qui veulent rejoindre ce convoi à Arudy, il passera à **l’espace Laprade vers 10h30** (juste après le viaduc de l’entrée d’Arudy).
  2. Convoi un peu plus rapide, qui ira jusqu'au supermarché de Laruns avant de revenir jusqu’au lac de Castet vers 12h (28km avant 12h, et au total 40km sur la journée). Des pneus trop fin ne seront pas adaptés à ce convoi qui empruntera des chemins après Castet.
* Au lac de Castet nous prendrons notre repas, soit sorti du sac, soit proposé par le bien connu food truck bio local la Fourgonetta.
* À partir de **14h** nous pourrons participer aux ateliers, expositions et animations, concert du **[Pic Vert festival](https://www.eteossalois.fr/pic-vert-festival/)** sur les thèmes de la biodiversité et la protection de l’environnement, la préservation de nos cadres de vie, de l’énergie, la gestion des déchets, les mobilités douces. **A 14h30 notamment, une conférence débat autour des mobilités douces sera menée**.   
L’association AF3V, Pau à Vélo, le CoCoTiers et le collectif Vallée Partagée tiendront un **stand commun** où chacun pourra partager sa vision du vélo, des aménagements cyclables, et de la chance d’accueillir une belle voie verte dans la Vallée d’Ossau. Nous espérons la présence d’élus de la vallée pour échanger sur ce thème.
* L’horaire du train de retour est à 17h05 pour une **arrivée à Pau à 17h26**.

Vu son intérêt aussi bien pour les pratiquants que pour l’attractivité et l’économie de la vallée, le projet d’une voie verte en continu de Buzy à Laruns sur l’emprise de l’ancienne voie ferrée désaffectée, magnifique initiative de la Communauté de Communes de la vallée d’Ossau a été initialement approuvé à l’unanimité des communes traversées et a reçu les financements du département, de la région, de l’Europe, du Feder.

La première partie, déjà réalisée, d’une douzaine de kilomètres est très agréable, dans un beau cadre de montagne (avec très peu de dénivelé) et des aménagements de qualité (enrobé lisse, tables de pique-nique en pierre d’Arudy, informations, postes de gonflage et réparation vélos, traversées sécurisées).

![](bitume.jpg)
  
Cette belle réalisation s’interrompt au rond-point Laspalettes, sur la commune de Bielle. La suite nécessite un aménagement non réalisé à ce jour.

![](chemin.jpg)

Cette randonnée festive veut **remettre ce projet sur le devant de la scène en montrant l’appétit du public pour cet itinéraire, et pour sa finalisation sur les tronçons manquants**.

La voie verte est un espace partagé. Les piétons, les cyclistes, les rollers, les coureurs, les personnes en fauteuil roulant, les familles avec poussette, les animaux et les engins agricoles devraient y trouver leur place en respectant l'autre.

**Une journée à ne pas manquer!**

