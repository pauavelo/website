---
title: "Les roues de l'avenir"
publishdate: 2023-12-09T02:09:32+01:00
author: "Sébastien Lamy"
date: 2023-12-15
lieu: "Cinéma le Méliès"
horaire: 20h
tags:
---

Après la [déambulation lumineuse](/agenda/2023/deambulation-lumineuse/), vous pouvez terminer la soirée par la projection du film "Les roues de l'avenir", à 20h, au cinéma le Méliès, en présence de Baptiste Lemaitre.

<!--more-->

Ce film aborde la place du vélo dans notre société et questionne les différentes manières d'en faire un acte majeur de la transition environnementale et sociétale. Une réflexion pour faire avancer le débat et imaginer l'avenir du vélo quotidien. La projection aura lieu en présence de Baptiste Lemaitre, porte parole du film, qui pourra répondre aux questions en fin de séance.

{{< youtube "pSrkBwjojhg" >}}

