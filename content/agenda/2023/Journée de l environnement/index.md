---
title: "Journée de l'environnement"
publishdate: 2023-09-15T10:16:32+02:00
author: "Sébastien Lamy"
date: 2023-09-17T09:30:00+02:00
lieu: "Château de Franqueville, Bizanos"
horaire: 09h30
tags:
  - balade
  - stand
---

Ce dimanche c'est la fête de l'environnement au château de Franqueville, à Bizanos. Avec le Conservatoire des Espaces Naturel (CEN), nous vous proposons une balade à vélo le matin, accompagnés de connaisseurs passionnés, pour aller observer libellules et papillons.

<!--more-->

**Le départ de la balade est à 10h au château de Franqueville, et nous vous proposons un rendez-vous à 9h au square Georges V à Pau, pour arriver ensemble au petit déjeuner offert au château** (à 9h30, juste avant la balade).

Toute la journée au château, nous tiendrons un stand aux côtés de plusieurs autres associations,

Le programme propose aussi de nombreuses conférences sur place. Toutes les infos en téléchargeant le programme (cliquez sur l'image ci-dessous).

<div class="pure-g trombi">
{{< dossier id="programme-journee-environnement" title="" >}}
</div>

Au plaisir de vous y retrouver.
