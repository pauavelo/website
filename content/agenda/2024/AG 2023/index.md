---
title: "AG 2023"
publishdate: 2024-01-17T23:52:18+01:00
author: ""
date: 2024-02-07T20:00:00+01:00
lieu: "MJC Berlioz, 84 av. de Buros, Pau"
horaire: 20h
tags:
---

Réservez votre soirée du mercredi 7 février : c'est l'Assemblée Générale de Pau à Vélo à 20h à la MJC
Berlioz.

<!--more-->

L'AG est ouverte à tous, une bonne occasion d'ailleurs de découvrir
notre association !

Adhérent·es, vous recevrez votre convocation et pouvoir par email sous peu.

Au plaisir de vous voir le 7 février,
