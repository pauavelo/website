---
title: "Bourse aux vélos"
publishdate: 2024-04-15T14:34:54+02:00
author: "Sébastien Lamy"
date: 2024-05-04T10:00:00+02:00
lieu: "Place Royale, Pau"
horaire: Dépôt 10h-12h30, Achat 13h30-16h
tags:
  - bourse aux vélos
---

Besoin de changer de vélo ? Venez acheter ou vendre le vôtre le 4 mai sur la place Royale à Pau !

<!--more-->

* **Le matin, de 10h à 12h30 nous recevrons les vendeurs de leur vélo
d’occasion** (2 maximum par vendeurs, à 150 € au maximum). Les vendeurs
fixent leur prix, et Pau à Vélo ne touche aucune commission.

* **Les acheteurs seront invités à essayer puis acheter leur vélo à partir
de 13h30 et jusqu'à 16h.**

Les vendeurs viendront récupérer la valeur de leur vélo, ou leur vélo
invendu entre 16h et 17h.

**Pièce d'identité obligatoire pour les vendeurs et les acheteurs qui
voudraient payer par chèque.** 

Vous pouvez consulter [le règlement de cette bourse aux vélos](reglement2024.pdf)

Nous vous attendons nombreux !  
Faites passer l'info !

