---
title: "Boulevard aux enfants"
publishdate: 2024-04-15T10:26:12+02:00
author: "Sébastien Lamy"
date: 2024-05-04T14:00:00+02:00
lieu: "Boulevard des Pyrénées, Pau"
horaire: 14h-23h
tags:
  - rue aux enfants
  - zone piétonne
  - boulevard des Pyrénées
  - jeux
  - expo
  - conte
  - animations
  - proposition
---

Venez avec vos jeux, transat et parasols, pour découvrir le boulevard des Pyrénées transformé : sans circulation ni stationnement, mais toujours avec les cafés, les terrasses, les Pyrénées, et avec en plus des animations pour les enfants et le public !

**Entre le square George V et les grilles du château**

<!--more-->

## 14h-20h Boulervard aux enfants

### Animations

* **École de cirque** *<small>14h-16h, près de l'église St Martin, avec [Cirquenbul](https://www.facebook.com/Cirquenbul/)</small>*
* **École de skate** *<small>16h-19h, près de l'église St Martin, avec [Culture Glisse](https://cultureglisse.fr/)</small>*
* **Initiation au tango** *<small>14h scéance enfants/ado, 15h séance adulte, square Georges V, avec [Valdi Tango](https://www.valditango.com/)</small>*
* **Contes des Pyrénées** *<small>16h séance 1; 17h15 séance 2, square Georges V, avec Sergi Mahourat de la ciutat</small>*
* **Maquillage** *<small>14h-18h place Royale</small>*
* **Massages assis** *<small>14h-18h place Royale, avec [Healing Essential](https://healing-essential.fr/)</small>*
* **Atelier vélo** *<small>place Royale, 14h-18h, avec [l'Atelier Vélo Participatif et Solidaire](https://ateliervelopau.fr)</small>*
* **[Bourse aux vélo](/agenda/2024/bourse-aux-velos/)** *<small>place Royale, Dépot 10h-12h30, Achat 13h30-16h00</small>*
* **Jeux de plein air** (basket, badminton, mini ping pong, molki, draisiennes,…)
* **Jeux pour les tout petits**  *<small>14h-18h avec [à petits pas](https://www.apetitspas-pau.fr/)</small>*
* **Jeux adaptés au handicap** et accessible à tous *<small>14h30-18h place Royale avec [handisport](https://www.cdhandisport64.org/)</small>*
* **Piste aux vélos** *<small>près des grilles du château, animée par la police municipale de 14h à 17h30</small>*
* **Cours de danse hors les murs** *<small>14h30-15h30, exercices de barre sur la rambarde par [le labo](https://www.lelabo-pau.fr/)</small>*
* **Coloriages, jeux de société, panneaux à peindre, craies à disposition**
* **Exposition sur l'évolution de l'espace public** *<small>par [Rue de l'avenir](https://www.ruedelavenir.com/)</small>*
 

### Musique

* **Aquiu Project** *<small>14h-16h, place royale</small>*
* **Bebop** *<small>14h-16h, déambulation</small>*
* **L'étoile métisse** (batucada) *<small>16h-18h, déambulation</small>*
* **Lo truc** (musique trad jouée par des enfants) *<small>18h-20h, déambulation emportée par [les triporteurs du gave](https://letriporteurdugave.github.io/)</small>*
* **Les dames de la poste ANNULE** *<small>18h-20h, annulé cause météo défavorable</small>*


## 20h-23h Boulevard en terrasse

De 20h à 23h, le boulevard des Pyrénées restera libre de la circulation motorisée et du stationnement, entre le square George V et les grilles du château. L'occasion de s'y promener ou d'y boire un verre dans une atmosphère apaisée !


## Et après ?

Ce changement de visage de l'espace public, nous espérons vous donner envie de le voir perdurer ! L'association Pau à Vélo propose depuis de nombreuses années la piétonisation du Boulevard des Pyrénées, promenade emblématique de la ville, avec ses nombreux cafés et terrasses. Quoi de plus souhaitable que d'en profiter à l’abri de la circulation motorisée, dans un espace public adapté à la marche et à la flânerie ?

Cette suggestion fait partie d'une proposition plus vaste portée par notre association : l'extension de la zone piétonne de Pau, dont vous pouvez voir la description dans [le manifeste pour la ville de Pau](https://municipales2020.parlons-velo.fr/manifeste/download/24880), rédigé à l'occasion des élections municipales de 2020.

![plan de la proposition d'extension de la zone piétonne de Pau](zone-pietonne-etendue-pau.svg)
