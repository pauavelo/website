---
# mettre l'année du CA (ex: "2018")
title: "2023"

# mettre la date de nomination du CA (l'ag)
date: 2023-02-02
# trombi:
# - "portraits": fabrique le trombi à partir des photos des membres listés.
#   Chaque photo doit avoir comme nom de fichier le nom du membre,
#   tout en minuscule sans accent, les espaces sont remplacés par
#   des tirets. ex: Jean Dùpont aura la photo jean-dupont.jpg ou jean-dupont.png
# - "trombi.jpg": le trombi est déjà mis en page dans une image nommée "trombi.jpg"
#   vous pouvez préciser un autre nom de fichier
# - "liste": il n'y aura pas de photos, la listes des membres et de leur fonction
#   sera affichée.
# Dans tous les cas l'image du trombi ou les photos des membres doivent
# se trouver à la racine du répertoire qui contient ce "_index.md"
trombi: "portraits"
trombi_class: "trombi"

# membres: décommenter pour saisir la liste des noms et fonctions des membres
# du CA. Ces noms seront utilisés pour construire un trombi automatiquement
# avec les photo si "autotrombi" vaut "true"
membres:
  - nom: "Antoine Lefeuvre"
    role: secrétaire - référent aménagements
  - nom: "Aurélie Erb"
    role: référente action
  - nom: "Bertrand Ottmer"
    role: administrateur - référent animations
  - nom: "Christophe Nussbaumer"
    role: administrateur
  - nom: "Françoise Seger"
    role: administratrice
  - nom: "Josette Castera"
    role: administratrice
  - nom: "Julie Champagne"
    role: trésorière
  - nom: "Julien Garnier"
    role: référent aménagement
  - nom: "Michel Barrère"
    role: administrateur
  - nom: "Pierrick Hospital"
    role: trésorier - référent voyage et AF3V
  - nom: "Sébastien Lamy"
    role: secrétaire (relations publiques)
  - nom: "Victor Dorland"
    role: secrétaire (réunions publiques)
---
