---
title: Imports des médias
date: 2024-12-29T17:42:36.801Z
draft: true
author: Sébastien Lamy
imageGroupClass: images
---

Pour importer des photos, des audios, des fichiers pdf à mettre dans votre article, le plus pratique est de copier les fichiers que vous voulez utilisez dans le dossier "import", puis, dans Visual studio code, de cliquer sur le bouton `Import des fichiers`.

<!--more-->

![](bouton-import.png)

* Attention à ne placer **que des copies** dans le dossier `import`. Les images en particulier sont transférées dans votre article en les transformant dans un format qui convient pour le web, donc avec une perte de qualité. L'image originale placée dans le dossier `import` est supprimée, ce qui peut être fâcheux si cette image était le seul original dont on disposait.
* Pour **tous les fichiers qui ne sont pas des images** (audio, pdf,...) Il faut les placer à la **racine du dossier `import`**. Lorsque vous cliquerez sur `Import des fichiers`, il seront importés dans le dossier de votre article, et le logiciel vous proposera le code à insérer dans votre article pour y placer ces fichiers

### Import des images

#### Image dans le corps de l'article
Pour intégrer simplement une image dans votre article, placez une copie de cette image à la racine du dossier `import`. L'image sera transformée pour convenir au web (réduction de la taille du fichier) Le code proposé permettra de mettre l'image en pleine largeur.

Si vous souhaitez conserver la qualité originale de votre image, et qu'elle peut convenir pour le web sans transformation (taille de fichier de moins de 500ko si possible), vous pouvez placer une copie de l'image dans le dossier `no-magick`. Les images dans ce dossier sont importées sans transformation et sans déperdition de qualité. Le code proposée permettra de mettre l'image en pleine largeur dans votre article.

#### Image d'en-tête

Les articles peuvent (et doivent si possible) avoir une image d'en-tête. C'est l'image qui est utilisée pour la miniature résumée de l'article (dans la page qui liste les articles), et c'est aussi l'image qui est affichée tout en haut en grand quand on affiche l'article dans son intégralité.

* **Nommage** : l'image d'en-tête doit obligatoirement s'appeler `header`. Enregistrez une copie de cette image à la racine du dossier `import`, sous le nom `header` (l'extention peut être `.jpg` ou `.png`), 
* **Ratio** : Lors de l'import, cette image sera transformée pour rentrer dans un rectangle de 1215x560 pixels. Pour qu'elle occupe toute la place (c'est souvent plus esthétique), le ratio longueur/largeur de l'image originale doit être de **2,17**. Si vous n'avez pas de logiciel qui permet simplement de cadre avec un ratio précis, le logiciel "Qimgv" est un logiciel très léger qui a cette capacité (en appuyant sur la touche `X` une fois que l'image est ouverte dedans)

#### Galerie standard
Pour importer un lot d'image à présenter en galerie (ensemble de miniature qu'on peut cliquer pour afficher puis défiler entre les images), il suffit de les placer ensemble dans un dossier que vous placerez à la racine du dossier `import`. Le nom de ce dossier importe peu. Je l'appelle souvent `photos`.

#### Galerie détaillée
Pour importer une ou plusieurs images qui s'afficheront en plein écran lorsqu'on clique dessus, il faut les placer dans le sous-dossier `images` qui se trouve dans le dossier `import`. Les images de ce dossier seront transféré dans un sous-dossier de l'article nommé `images`. Par défaut, elle ne seront pas miniaturisées dans l'article. Le code proposé après l'import vous permettra de rajouter une légende pour chacune, et de choisir la largeur à laquelle chacune sera affichée dans l'article. Vous pouvez consulter le détail de [la syntaxe d'un groupe d'image à zoomer dans votre article](/aide/syntaxe#groupe-dimages-légendables-à-zoomer). La syntaxe pour obtenir une galerie de miniature détaillée se trouve juste après.


### Import des pdf
Lorsque vous placez un pdf dans le fichier `import`, visual studio vous pose deux questions lorsque vous cliquez sur le bouton `import des fichiers`.
* Est ce que le fichier est réalisé par Pau à Vélo ? Si vous répondez oui, le fichier sera mieux mis en valeur, car il sera présenté sous la forme d'une image miniature de la première page, sur laquelle on peut cliquer pour télécharger le fichier. Sinon ce sera un simple lien textuel vers le fichier.
* Choisissez un nom de fichier qui explicite bien le sujet dont parle le document. Ne mettez pas "Pau à vélo" ni "Dossier" dans ce nom. Exemple de nom : "Contribution au PLUI", ou "Alsace Lorraine". Un nom explicite, c'est toujours plus sympa pour les moteurs de recherche. Si vous avez répondu "oui" à la première question, les mots "pau-velo-dossier" seront rajouté au nom du fichier.


### Import des vidéos
Les vidéos sont des fichiers très volumineux, et notre site n'est pas prévu pour les accueillir directement. Ne placez pas de vidéo dans le dossier `import`, préférez utliser notre compte Viméo, et intégrer ensuite dans l'article [le code Viméo](/aide/syntaxe#vimeo-ou-youtube).