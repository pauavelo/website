---
title: Syntaxe
date: 2024-12-29T17:42:36.801Z
draft: true
author: Sébastien Lamy
imageGroupClass: images
toc: "true"
---

Retrouvez dans cette article la syntaxe pour afficher des éléments spéciaux dans votre article (images, audio, pdf, bouton-lien..).

<!--more-->

## Images

> **Bon à savoir**
> Utilisez le dossier `import` et le bouton `import des fichiers` depuis votre article, dans visual studio code, pour importer les images dans le dossier de votre article, et obtenir le code pour les insérer. Cette procédure garantit que les images seront transformées dans un format adapté au web (notamment limiter la taille de fichier). Retrouvez les convention du dossier `import` sur [la page dédiée](/aide/import/)

### Image d'en tête
L'image d'en-tête de l'article est celle qui est représentée en miniature dans le résumé de l'article, et affichée en grand en haut de l'article. Par convention cette image doit se trouver dans le dossier de votre article, et se nommer `header.jpg`. Pour qu'elle remplisse tout l'espace qui lui est prévu (souvent c'est plus joli), il faut qu'elle ait un ratio longueur/largeur de 2,17, et une largeur minimum de 1215px.

Certaines images d'en tête sont réutilisée entre plusieurs articles. Dans ce cas, il faut mettre l'image dans le dossier `content/media`, et préciser son nom dans le _frontmatter_[^1] de l'article. Par exemple si votre image réutilisable s'appelle `ag.jpg`

```yaml
header: "ag.jpg"
```

### Image dans le corps de l'article
```md
![](photos/2024-08-04-14h42m25s-img-1666.jpg)
```
![](photos/2024-08-04-14h42m25s-img-1666.jpg)

Pour afficher une image en pleine largeur dans l'article

> **Bon à savoir**
> Utilisez le dossier `import` et le bouton `import des fichiers` depuis votre article, dans visual studio code. Cette procédure place les fichiers au bon endroit, vous propose le code à insérer dans votre article, et garantit que les images seront transformées dans un format adapté au web. Retrouvez les convention du dossier `import` pour les image en pleine largeur dans un article sur [la page dédiée](/aide/import/#image-dans-le-corps-de-larticle)


### Galerie d'images

```html
{{</* gallery photos */>}}
```
{{< gallery photos >}}

Montre l'ensemble des photos présentes dans un sous-répertoire de l'article. Dans l'exemple ci-dessus, le sous-répertoire se nomme `photos`

> **Bon à savoir**
> Utilisez le dossier `import` et le bouton `import des fichiers` depuis votre article, dans visual studio code. Cette procédure place les fichiers au bon endroit, vous propose le code à insérer dans votre article, et garantit que les images seront transformées dans un format adapté au web. Retrouvez les convention du dossier `import` pour les galeries d'images sur [la page dédiée](/aide/import/#galerie-standard)

### Image légendable à zoomer
```html
{{</* image src="photos/2024-08-04-14h42m25s-img-1666.jpg" 
          caption="un joli bout de campagne" */>}}
```

<div class="images">
{{<image src="photos/2024-08-04-14h42m25s-img-1666.jpg" caption="un joli bout de campagne">}}
</div>


> **Bon à savoir**
> Utilisez le dossier `import` et le bouton `import des fichiers` depuis votre article, dans visual studio code. Cette procédure place les fichiers au bon endroit, vous propose le code à insérer dans votre article, et garantit que les images seront transformées dans un format adapté au web. La convention pour d'import pour les images à zoomable est celle de la galerie détaillée. Retrouvez cette convention dans [la page dédiée](/aide/import/#galerie-détaillée)


### Groupe d'images légendables à zoomer


```yaml
imageGroupClass: images
---
```
```html
<div class="images">
{{</* image src="img/structure_repondants_age.png" 
          caption="selon l'âge" */>}}
{{</* image src="img/structure_repondants_genre.png" 
          caption="selon le genre" */>}}
{{</* image src="img/structure_repondants_usage.png" 
          caption="selon l'usage (Part des répondants ayant cité l'usage)" */>}}
</div>
```

<div class="images">
{{< image src="img/structure_repondants_age.png" caption="selon l'âge" >}}
{{< image src="img/structure_repondants_genre.png" caption="selon le genre" >}}
{{< image src="img/structure_repondants_usage.png" caption="selon l'usage (Part des répondants ayant cité l'usage)" >}}
{{< image src="img/structure_repondants_frequence.png" caption="selon la fréquence d'usage du vélo" >}}
{{< image src="img/structure_repondants_niveau.png" caption="selon le niveau de pratique" >}}
{{< image src="img/structure_repondants_tolerance.png" caption="selon la tolérance au trafic (Réponse à la question: être séparé de la circulation motorisée est important)" >}}
</div>

_Cliquez sur une image pour l'agrandir et voir sa légende s'afficher en bas. Vous pouvez ensuite utiliser les flèches pour défiler entre les images du groupe._

Par défaut il n'y a pas besoin de l'encadrement avec les balises `<div> [...] </div>` : dans l'ensemble de l'article, toutes les images insérées, avec le code `{{</* image src="..." */>}}` sont dans le même groupe, même si elles ne sont pas côte à côte. Cependant cette configuration ne marche plus si on trouve en plus dans l'article une galerie de miniatures (comme c'est le cas [ici dans cette article d'aide](#galerie-dimages)). Dans ce cas, les images zoomables qu'on veut regrouper doivent être entre deux balises `
```html
<div class="...">
     [...]mes images a regrouper, 
     qui peuvent être entrecoupée de texte si je veux 
     [...]
</div>
```
La valeur pour l'attribut `class` est libre, il faut juste éviter le mot `gallery`, qui est réservé à un groupe de miniatures sans légendes. Une fois la valeur choisie, il faut la signaler dans le _frontmatter_[^1] de l'article. Par exemple si j'ai mis `<div class="images">`, il faudra mettre dans le _frontmatter_

```yaml
imageGroupClass: images
```

S'il y a plusieurs groupes d'images dans votre article, vous pouvez leur affecter tous le même nom de classe (pas besoin de mettre un nom de classe différent pour chaque groupe d'images). Il n'y a donc besoin que d'une seul ligne dans le _frontmatter_, quelque soit le nombre de groupes d'images dans l'article.

### Galerie de miniatures légendées à zoomer
```yaml
imageGroupClass: gallery
---
```

```html
<div class="gallery">
{{</* image src="vv/plan.png" width="300"
          caption="Pau plan proposé pour le rond-point du Souvenir Français (foire expo)"  */>}}
{{</* image src="vv/souvenir-francais.jpg" width="300"
          caption="Pau rond point du Souvenir Français (foire expo)"  */>}}
{{</* image src="vv/gramont-pieton.jpg" width="300"
          caption="Pau place Gramont piétonne"  */>}}
{{</* image src="vv/lycee-barthou-pieton.jpg" width="300"
          caption="Pau lycée Louis Barthou piéton"  */>}}
{{</* image src="vv/barthou-pieton.jpg" width="300"
          caption="Pau rue Louis Barthou piétonne"  */>}}
{{</* image src="vv/alsace-lorraine.jpg" width="300"
          caption="Pau boulevard Alsace-Lorraine"  */>}}
</div>
```

<div class="gallery">
{{< image src="vv/plan.png" caption="Pau plan proposé pour le rond-point du Souvenir Français (foire expo)" width="300" >}}
{{< image src="vv/souvenir-francais.jpg" caption="Pau rond point du Souvenir Français (foire expo)" width="300" >}}
{{< image src="vv/gramont-pieton.jpg" caption="Pau place Gramont piétonne" width="300" >}}
{{< image src="vv/lycee-barthou-pieton.jpg" caption="Pau lycée Louis Barthou piéton" width="300" >}}
{{< image src="vv/barthou-pieton.jpg" caption="Pau rue Louis Barthou piétonne" width="300" >}}
{{< image src="vv/alsace-lorraine.jpg" caption="Pau boulevard Alsace-Lorraine" width="300" >}}
</div>

_Cliquez sur une image pour l’agrandir et voir sa légende s’afficher en bas. Vous pouvez ensuite utiliser les flèches pour défiler entre les images du groupe._


Quand on insère un dossier de photos avec la syntaxe `{{</* gallery photos */>}}`, on ne peut pas donner de légendes aux images. Pour afficher une légende pour une ou plusieurs images, il faut tout détailler manuellement, en utilisant le code `{{</* image src="" caption="" width="" */>}}` pour chaque image
* `src` : le chemin vers le fichier d'image, dans le répertoire de l'article
* `caption` : la légende pour cette image
* `width`: la largeur de la miniature à afficher, en pixel. Pour les miniatures, 300 est une bonne valeur.

Pour afficher ces miniatures côte à côte sans leur légende, il faut les placer entre des balises `<div class="gallery">` et `</div>`. C'est le mot-clé `gallery` pour l'attribut `class` qui aura pour effet de ne pas afficher les légendes et de placer les miniatures côte à côte.

Aussi, pour que les miniatures s'affichent joliment arrangées les une par rapport aux autres, il faut déclencher le script qui les arrange. Ceci se fait en plançant dans le _frontmatter_[^1] de l'article la ligne

```yaml
imageGroupClass: gallery
```
 Cette ligne peut être oubliée si l'article contient par ailleurs une [galerie d'image standard](#galerie-dimages), car le script qui arrange la galerie standard arrangera aussi cette galerie détaillée.

> **Bon à savoir**  
> Utilisez le dossier `import` et le bouton `import des fichiers` depuis votre article, dans visual studio code. Cette procédure place les fichiers au bon endroit, vous propose le code à insérer dans votre article, et garantit que les images seront transformées dans un format adapté au web. La convention pour d'import pour une galerie de miniature détaillée est celle de la galerie détaillée. Retrouvez cette convention dans [la page dédiée](/aide/import/#galerie-détaillée)

## Fichier pdf

### Lien textuel simple
Pour afficher un simple lien textuel vers un fichier pdf

```html
[Dossier Henri Faisans](henri-faisans.pdf)
```
[Dossier Henri Faisans](henri-faisans.pdf)


### Lien avec une image miniature



```html
<div class="pure-g trombi">
 {{</* dossier id="henri-faisans"
             title="Réponse sur Henri Faisans" 
             author="Jocelyn Mounié"
             legend="Le point de vue de notre asso"  */>}}
</div>
```

<div class="pure-g trombi">
 {{< dossier id="henri-faisans" title="Réponse sur Henri Faisans" author="Jocelyn Mounié" legend= "Le point de vue de notre asso" >}}
</div>

Ce lien avec une image miniature met mieux en valeur le document, **il est à réserver aux pdf qui sont produits par notre association**.

* `id` : permet à la fois de déterminer le nom du fichier pdf, et le nom du fichier d'image miniature. Par exemple ici `id` est `henri-faisans`, donc le fichier pdf doit s'appeler `pau-velo-dossier-henri-faisans.pdf`, et l'image miniature doit s'appeler `henri-faisans.jpg`.
* `title` : _(facultatif)_ Le titre à afficher au dessus de la miniature. 
* `author` : _(facultatif)_ L'auteur du dossier, si on souhaite le préciser.
* `legend` : _(facultatif)_ Un texte à rajouter sous l'image pour donner des précisions.
* Le fait de placer le lien dans une `<div class="pure-g trombi">` n'est pas obligatoire, mais permet d'afficher l'image bien centrée, et si on a plusieurs fichiers pdf à proposer, d'afficher les miniatures côte à côte.

> **Bon à savoir**  
> Utilisez le dossier `import` et le bouton `import des fichiers` depuis votre article, dans visual studio code. Cette procédure place les fichiers au bon endroit, vous propose le code à insérer dans votre article. La convention pour d'import pour les fichiers `pdf` est détaillée sur [la page dédiée](/aide/import/#import-des-pdf)

## Openrunner

```html
{{</* openrunner id="20466778" img="parcours.jpg" */>}}
```

{{< openrunner id="20466778" img="parcours.jpg" >}}


Lien vers un parcours sur openrunner

* `id` l'id du parcours sur openrunner, visible dans l'url qui vous est proposée par openrunner pour le partage
* `img` l'image à afficher pour le lien vers openrunner. Vous pouvez oublier cet attribut si votre image s'appelle `openrunner.jpg`. Si elle porte un nom différent, cet attribut permet de le préciser.

> **Bon à savoir**  
> Si vous placez une image nommée `openrunner` dans le dossier d'import (à la racine, ou dans le sous-dossier `no-magick`), puis que vous cliquez sur `Import des fichiers` depuis votre article, le code pour insérer un lien vers un parcours openrunner vous sera proposé. Il faudra compléter ce code avec l'`id` du parcours.

## Vimeo ou Youtube

```html
{{</* vimeo 479137265 */>}}
```
{{< vimeo 479137265 >}}

```html
{{</* youtube WAwA5cHuL9s */>}}
```

{{< youtube WAwA5cHuL9s >}}

## Audio

```html
{{</* audio src="2024-11-19-france-bleu-pauavelo.mp3"
          caption="Extrait du journal de 8h du 19/11/2024 sur France Bleu" */>}}
```

{{< audio src="2024-11-19-france-bleu-pauavelo.mp3"
          caption="Extrait du journal de 8h du 19/11/2024 sur France Bleu" >}}


## Bouton lien

```html
{{%/* button href="https://adhesions.pauavelo.fr/nouvelle"
           label="Adhérer à Pau à Vélo" */%}}
```

{{% button href="https://adhesions.pauavelo.fr/nouvelle"
           label="Adhérer à Pau à Vélo" %}}

Dans visual studio code, vous pouvez insérer ce bout de code en cliquant sur l'icône des petits ciseaux en haut à droite.

## Table des matières

La table des matières reprends tous les titres de niveau 2 et 3 de votre article (comme la table des matière de cette article). Pour insérer une table des matières en début d'article, il suffit d'écrire dans le _frontmatter_[^1] de l'article la ligne:

```yaml
toc: "true"
```

## Guidon
```html
<div class="pure-g trombi">
<!-- On a choisi l'id "jean-yves", donc il faut un pdf nommé 
     "pau-velo-guidon-dor-dossier-jean-yves.pdf" 
     et une impage de prévisualisation nommée "jean-yves.png" -->
{{</* guidon  id="jean-yves" 
            title="Billère" 
            subtitle="av. du Château d'Este" 
            author="Jean-Yves Deyris" 
            score="7" */>}}
{{</* guidon  id="annick" 
            title="Mazères-Lezons" 
            subtitle="rue de la République" 
            author="Annick Potier" 
            score="7" */>}}
</div>
```

<div class="pure-g trombi">
{{< guidon id="jean-yves" title="Billère" subtitle="av. du Château d'Este" author="Jean-Yves Deyris" score="7" >}}
{{< guidon id="annick" title="Mazères-Lezons" subtitle="rue de la République" author="Annick Potier" score="5" >}}
</div>

Très similaire au lien vers un pdf avec une miniature, ce shortcode a été mis en place pour [présenter les guidons d'or en 2017](/blog/2017/palmares-du-guidon-dor-2017/). 

* `id` : permet de déterminer à la fois le nom du fichier pdf et le nom de la miniature. Cette `id` doit être en rapport avec le contenu du fichier, pour aider les moteur de recherche. Dans l'exemple ci-dessus le guidon qui a l'`id` `jean-yves` correspond au fichier `pau-velo-guidon-dor-dossier-jean-yves.pdf`, et à la miniature `jean-yves.png`. Je recommande de ne pas dépasser 320px de large ou de haut pour cette image miniature.
* `title` : le titre à afficher au dessus de la miniature pour ce dossier.
* `author`: l'auteur de ce dossier
* `score`: le nombre de vote pour ce dossier


[^1]: le _frontmatter_ de l'article est l'en-tête de l'article : la partie tout en haut qui contient les métadonnées (titre, date, auteur, tags...)
