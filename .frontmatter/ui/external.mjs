//import path from 'path';

import {
  enableDevelopmentMode,
  registerCardImage,
  registerCardFooter,
  registerPanelView,
  registerCustomField
} from "https://cdn.jsdelivr.net/npm/@frontmatter/extensibility/+esm"

//from '@frontmatter/extensibility';

//enableDevelopmentMode();

/**
 * @param {string} filePath - The path of the file
 * @param {object} data - The metadata of the file
 * @returns {string} - The HTML to be rendered in the card image
 */
registerCardImage(async (filePath, metadata) => {
    console.log(metadata)
    if (["blog","agenda","balade"].includes(metadata.fmFolder)){
    var relpath = metadata.header ? `content/media/${metadata.header}` : metadata.fmRelFileWsPath.replace("[[workspace]]/",'').replace(/_{0,1}index.md$/,'header.jpg')
    const image = metadata.fmPreviewImage ? metadata.fmPreviewImage : metadata.fmWebviewUrl+'/'+relpath;
    return `<img src="${image}" alt="${metadata.title}" style="object-fit: cover;" class="h-36" />`;
    //return `<span>${image}</span><span>${JSON.stringify(metadata)}</span>`
    }
});