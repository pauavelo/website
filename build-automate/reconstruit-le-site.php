<?php
# pour appeler cette page avec le bon token en header
# curl -H "X-GITLAB-TOKEN:C'est bon, je suis le gitlab p0avel0, j'ai le droit\!" https://pauavelo.fr/admin/reconstruit-le-site.php
$log_dir = "/home/oafo2293/pauavelo.fr/build-automate/logs";
# on vérifie qu'il y a le mot de passe secret
if ($_SERVER['HTTP_X_GITLAB_TOKEN'] == "C'est bon, je suis le gitlab p0avel0, j'ai le droit!")  {
	# on execute le script en arrière plan car il prend du temps, inutile de bloquer gitlab
	shell_exec(sprintf('%s > "'.$log_dir.'/'.date("Y-m-d H\hi\ms\s").'" 2>&1 &', "./build_site.sh"));
} 
else {
    header('WWW-Authenticate: Basic realm="My Realm"');
    header('HTTP/1.0 401 Unauthorized');
    # Pour afficher le header reçu
    # print_r($_SERVER);
    echo "{$_SERVER['HTTP_X_GITLAB_TOKEN']} -> T'as pas le bon mot de passe, donc t'as pas le droit !";
    exit;
}
?>
