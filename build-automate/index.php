<?php 
	$file = array_pop(scandir(__DIR__.'/logs',)); 
	if ($file != ".."){$url="logs/$file";} else {$url="empty";}
?>

<!DOCTYPE html>
<!-- Copyright (c) 2012 Daniel Richman; GNU GPL 3 -->
<html>
    <head>
        <title>build.pauavelo.fr</title>
        <script type="text/javascript" src="jquery.min.js"></script>
        <script type="text/javascript">var url="<?= $url ?>"</script>
        <script type="text/javascript" src="logtail.js"></script>
        <link href="logtail.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div id="header">
            <h1>Log de construction du site pauavelo.fr</h1>
            <!--
            	Pas besoin d'afficher ces liens 
            	<a href="./">Chronological</a> or
            	<a href="./?reverse">reversed</a> view.
            -->
            <a id="pause" href='#'>Pause</a> | <a href="logs/">Voir l'historique des logs</a>
        </div>
        <pre>Date de demarrage de ce processus : <?= $file ?></pre>
        <pre id="data">Loading...</pre>
    </body>
</html>
