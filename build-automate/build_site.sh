#!/bin/bash

# On évite d'avoir plusieurs instance de ce programme qui tournent en même temps
[ "${FLOCKER}" != "$0" ] && echo "flock: demande du verrou pour générer le site web..."
[ "${FLOCKER}" != "$0" ] && exec env FLOCKER="$0" flock -e --verbose "$0" "$0" "$@"

# Nécessaire de mettre à jour le path dans le cadre d'unne execution par php de hugo et de git-lfs
export PATH="$PATH:/home/oafo2293/bin/go/bin:/home/oafo2293/bin:/home/oafo2293/.local/bin"
cd ~/pauavelo.fr;

echo "recherche des mises à jour du code source sur gitlab...";
git remote update;
if (( `git rev-list --count master...master@{upstream}` > 0 )); then
  	if git pull; then
	  	echo "";
		if hugo -d temp; then
			echo "La construction du site à partir du code source a réussi !";
			echo "";
			echo "suppression de l'ancienne version..."
			rm -rf public;
			echo "mise en ligne de la nouvelle version..."
			mv temp public;
			echo "Terminé !";
		else
			echo "La construction du site a échoué !";
			echo "suppression des fichiers temporaires de construction...";
			rm -r temp;
			echo "Terminé !";
		fi
	else
		echo "Un problème est survenu lors de la mise à jour du code source, il va falloir mettre les mains dans le camboui !";
	fi
else
   echo "Rien n'a changé dans le code source par rapport à la version actuellement en ligne."
fi
