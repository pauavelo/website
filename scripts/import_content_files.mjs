const importFolder = './import/';
const headerSize = '1215x560';
const headerQuality = 85;
const contentImageSize = '1300x1300';
const contentImageQuality = {min:20,max:85};//si l'image est plus petite que la taille demandée, on utilise max, sinon min.
const gallerySize = '1200x1200';
const galleryQuality = 85;
const pdfpreviewMaxSize = 350;//La taille max pour l'aperçu des pdf (hauteur ou largeur)

import { ContentScript } from '@frontmatter/extensibility';
import fs from 'fs';
import isImage from 'is-image';
import path from 'path';
import sharp from 'sharp';
import { getDocument } from "pdfjs-dist/legacy/build/pdf.mjs";

// Des variables utiles pour extraires l'image à partir des pdf
// code copié collé de https://github.com/mozilla/pdf.js/blob/master/examples/node/pdf2png/pdf2png.mjs
const CMAP_URL = "../../../node_modules/pdfjs-dist/cmaps/";
const CMAP_PACKED = true;
const STANDARD_FONT_DATA_URL = "../../../node_modules/pdfjs-dist/standard_fonts/";

const { 
  workspacePath, // le dossier du projet depuis lequel on a déclenché l'import
  filePath,  // le chemin du document (fichier "index.md") qui veut importer les médias
  frontMatter, // le frontmatter du fichier "index.md"
  answers //les données saisies par l'utilisateur si on lui demande
} = ContentScript.getArguments(); 

const textToInsert = []; // le texte à insérer dans "index.md" pour faire référence aux fichiers importés
const pdfAsk = [];



// Enlève les caractère spéciaux, les accent, remplace _ et "espace" par des tirets
function slugify(str) {
  return str
    .toLowerCase()
    .trim()
    .normalize("NFD").replace(/[\u0300-\u036f]/g, "") // remove accent
    .replace(/[^\w\s\.-]/g, '') //remove non alphanumeric characters
    .replace(/[\s_-]+/g, '-') // remove spaces and underscores and dashes
    .replace(/^-+|-+$/g, ''); // remove dash at very beginnig/very end, if any
}


// Rajoute des infos utile à l'objet file de node.js
function extendFileInfos(file,{
  outputDir = path.dirname(filePath),
  parse = path.parse(file.name),
  fullPath = path.join(file.parentPath, file.name),
  outputName = slugify(parse.name),
  outputExt = parse.ext.toLowerCase()}={}) 
{
  file.outputDir = outputDir
  file.parse = parse
  file.fullPath = fullPath;
  file.outputName = outputName;
  file.outputExt = outputExt;
}

function outputPath(file){
  return(path.format({ dir: file.outputDir, name: file.outputName, ext: file.outputExt}));
}

// supprime un fichier du répertoire d'import
function rmFile(file) {
  fs.rmSync(file.fullPath);
}

// déplace un fichier depuis le répertoire d'import
function mvFile(file) {
  fs.renameSync(file.fullPath, outputPath(file));
}

// Traduit les paramètres de redimensionnement donnés sous la forme 'widthxheight'
function parseImageSize(str, withoutEnlargement = true) {
  let sizeArray = str.split("x").map(xyz => parseInt(xyz));
  return {
    width: sizeArray[0],
    height: sizeArray[1],
    withoutEnlargement: withoutEnlargement,
    fit: 'inside'
  };
}

// Donne la taille de l'image une fois qu'on a pris en compte
// les information d'orientation
function getNormalSize({ width, height, orientation }) {
  return (orientation || 0) >= 5
    ? { width: height, height: width }
    : { width, height };
}


/**
 * Renders the first PDF page to an image.
 * 
 * @param {FileObj} file - The pdf file.
 */
async function pdfToImage(file) {
  // Load the original PDF using pdf.js
  let pdfBuffer = new Uint8Array(fs.readFileSync(file.fullPath));
  let loadingTask = getDocument({ 
    data: pdfBuffer,cMapUrl: CMAP_URL,
    cMapPacked: CMAP_PACKED,
    standardFontDataUrl: STANDARD_FONT_DATA_URL, 
    verbosity:0
  });
  let pdfDocument = await loadingTask.promise;
  // Get the first page.
  let page = await pdfDocument.getPage(1);  
  let viewport = page.getViewport({ scale: 1, });
  // Define the scale
  let scaleX = pdfpreviewMaxSize / viewport.width;
  let scaleY= pdfpreviewMaxSize / viewport.height;
  let scale = Math.min(scaleX,scaleY);
  // Render with this scale
  let scaledViewport = page.getViewport({ scale: scale });
  let canvasFactory = pdfDocument.canvasFactory;
  let canvasAndContext = canvasFactory.create(
    scaledViewport.width,
    scaledViewport.height
  );
  let renderContext = {
    canvasContext: canvasAndContext.context,
    transform: null,
    viewport: scaledViewport
  };

  let renderTask = page.render(renderContext);
  await renderTask.promise;
  // Convert the canvas to an image buffer.
  let imageBuffer = canvasAndContext.canvas.toBuffer("image/jpeg");
  let outputImagePath = path.format({ dir: file.outputDir, name: file.outputName, ext: ".jpg" });
  fs.writeFileSync(outputImagePath, imageBuffer);
}

// Rajoute les questions afférentes à un pdf à la liste des questions à poser
function pushPdfQuestion(file){
  pdfAsk.push({
    name: `"${file.name}___homeMade"`,
    message: `Est ce que le fichier « ${file.name} » est réalisé par Pau à Vélo ?`,
    options: ["oui", "non"]
  });
  pdfAsk.push({ 
    name: `"${file.name}___title"`,
    message: `Choisissez un nom de fichier qui explicite bien le sujet
    dont parle le document « ${file.name} ». Ne mettez pas "Pau à vélo" 
    ni "Dossier" dans ce nom. Exemple de nom : "Contribution au PLUI", 
    ou "Alsace Lorraine`,
    default: file.parse.name
  });
}


// importe les pdfs en fonctions des réponses apportées au question
function importPdfs() {
  let pdfParameters=[]//dans chaque case un hash de type{fileName:<string> titre:<string>, dossier:<bool>}
  var pdfParameter={}
  Object.entries(answers).forEach( ([key, value]) => {
    // on interprète chacune des réponses
    if (key.endsWith('___homeMade')){
      pdfParameter = {
        filename: key.replace(/___homeMade$/,''),
        dossier: (value==='oui') ? true : false
      }
    }
    else{
      pdfParameter.title = value
      pdfParameters.push(pdfParameter)
    }
  });
  // on traite chacune des réponses
  pdfParameters.forEach( pdfParameter => {
    let outputName = slugify(pdfParameter.title)
    let file = {
      name : pdfParameter.filename,
      parentPath : importFolder
    }
    extendFileInfos(file,{outputName:outputName})
    if (pdfParameter.dossier){
      pdfToImage(file).then( () => {
        file.outputName = (pdfParameter.dossier ? "pau-velo-dossier-" : "") + outputName
        mvFile(file)
      })
      textToInsert.push('<div class="pure-g trombi">')
      textToInsert.push(` {{< dossier id="${outputName}" title="" >}}`)
      textToInsert.push("</div>")
    }
    else{
      textToInsert.push(`[${pdfParameter.title}](${outputName}.pdf)`)
      mvFile(file)
    }
  })

}

// Le texte à insérer pour mettre une image dans l'article. On gère
// le cas spécifique de l'image d'un parcours openrunner, qui doit
// comporter un lien vers le site openrunner.
function imageRef(file){
  if (file.outputName == "openrunner"){
    return(`{{< openrunner id="" >}}`)
  }
  else{
    return(`![](${file.outputName+file.outputExt})`);
  }
}

// Importe une image situé dans un sous-dossier du dossier d'import
// Cette image est destinée à être affichée dans une galerie (une
// miniature parmi d'autres, on peut cliquer pour la voir en grand)
async function importGalleryImage(file){
  file.outputExt = ".jpg"
  let resizeOptions = parseImageSize(gallerySize)
  return sharp(file.fullPath)
    .withMetadata()
    .rotate()
    .resize(resizeOptions)
    .jpeg({quality: galleryQuality})
    .toFile(outputPath(file))
    .then( info => {rmFile(file)})
}


// Importe une image située à la racine du dossier d'import.
// Cette image est destinée à être affichée directement dans la page web, eelle
// est placée dans le même dossier que le document.
function importContentImage(file) {
  file.outputExt = ".jpg"
  if (file.parse.name == "header") {
    // on importe et redimensionne le header
    let resizeOptions = parseImageSize(headerSize);
    resizeOptions.fit = 'cover';
    sharp(file.fullPath)
      .withMetadata()
      .resize(resizeOptions)
      .jpeg({ quality: headerQuality })
      .toFile(outputPath(file))
      .then(info => { rmFile(file); });
  }
  else {
    // on importe et redimensionne une image pour l'article
    let outputQuality = contentImageQuality.max;
    let inputImage = sharp(file.fullPath);
    let resizeOptions = parseImageSize(contentImageSize);
    inputImage.metadata().then(metadata => {
      if (getNormalSize(metadata).width > contentImageSize.width) {
        quality = contentImageQuality.min;
      }
      inputImage
        .withMetadata()
        .rotate()
        .resize(resizeOptions)
        .jpeg({ quality: outputQuality })
        .toFile(outputPath(file))
        .then(info => { rmFile(file); });
    });
    textToInsert.push(imageRef(file))
  }
}

// pour les svg et le contenu du dossier "no-magick", 
// on met un lien de type image
function importRawImage(file) {
  mvFile(file);
  textToInsert.push(imageRef(file));
}

// Pour les mp3
function importAudio(file){
  mvFile(file)
  textToInsert.push(`{{< audio src="${file.outputName+file.outputExt}" caption="" >}}`)
}

// les fichiers de type inconnu sont copiés tel quel
function importUnknownType(file) {
  mvFile(file);
  textToInsert.push(`[](${file.outputName + file.parse.ext})`);
}


// Importe un fichier situé à la racine du répertoire d'import
function importFile(file) {
  switch (file.parse.ext.toLowerCase()) {
    case ".pdf": pushPdfQuestion(file); break;
    case ".svg": importRawImage(file); break;
    case ".mp3":
    case ".wav":
    case ".ogg":
      importAudio(file); break;
    default:
      if (isImage(file.name)) {
        importContentImage(file);
      }
      else {
        importUnknownType(file);
      }
  }
}


// Renvoie la fonction qu'il faudra appliquer à chaque fichier du répertoire
// pour importer ce fichier
function importFileFunction(dir) {
  if (dir.name == "no-magick") {
    // Les images dans ce dossier sont importées sans transformation
    // et dans le dossier "racine" du contenu
    return (noMagickFile => {
      extendFileInfos(noMagickFile);
      importRawImage(noMagickFile);
    });
  }
  else {
    // Les images de ce dossier sont importées dans un sous-répertoire
    // après transformation.
    return (galleryFile => {
      extendFileInfos(galleryFile);
      galleryFile.outputDir = path.join(galleryFile.outputDir, dir.outputName);
      dir.promises.push(importGalleryImage(galleryFile));
    });
  }

}

// Rajoute le texte à copier pour afficher les éléments importés dans la page web
function outputDirText(dir) {
  if (dir.name == "images") {
    // Pour ce dossier il faut une div englobante, et une ligne pour chaque fichier
    textToInsert.push('<div class="gallery">');
    dir.galleryFiles.forEach(galleryFile => {
      textToInsert.push(` {{< image src="images/${galleryFile.outputName}.jpg" caption="" width="" >}}`);
    });
    textToInsert.push('</div>');
  }
  else if (dir.name != "no-magick") {
    // pour les dossier "standard" on insère le shorcode `gallery`
    textToInsert.push(`{{< gallery ${dir.outputName} >}}`);
  }
}

// importe tous les fichiers d'un répertoire situé à la racine du répertoire d'import.
// Il y a deux répertoire particuliers : "images" et "no-magick"
function importDir(dir) {
  dir.galleryFiles = fs.readdirSync(path.join(importFolder, dir.name), { withFileTypes: true });
  dir.promises = [];
  if (dir.galleryFiles.length) {
    if (dir.name != "no-magick") {
      // on créée le repertoire de destination si nécessaire
      let outputDir = path.join(dir.outputDir, dir.outputName);
      if (!fs.existsSync(outputDir)) { fs.mkdirSync(outputDir); }
    }
    // pour chaque fichier, on applique la fonction d'import choisie pour le dossier
    dir.galleryFiles.forEach(importFileFunction(dir));
    // on ajoute le texte de sortie à copier coller pour l'utilisateur
    outputDirText(dir);
  }

  // on supprime le répertoire si nécessaire, après traitement de tous les fichiers contenus
  if (!["no-magick", "images"].includes(dir.name)) {
    Promise.all(dir.promises).then(info => { fs.rmdirSync(dir.fullPath); });
  }
}


function do_import(file) {
  // On compile des infos utiles
  extendFileInfos(file);
  if (file.isFile()) { importFile(file); }
  else { importDir(file); }
}


(() => {
  if(!answers){
    // on est dans l'execution initiale.
    var files = fs.readdirSync(importFolder, {withFileTypes:true})
    files.forEach(file => {do_import(file);});

    if (pdfAsk.length) {
      // on a trouvé des pdf
      // Comme on va relancer le script, il faut sauvegarder le texte de sortie.
      fs.writeFileSync('_temp_content_import_',textToInsert.join("\r\n"))
      // No answers found, ask the user
      ContentScript.askQuestions(pdfAsk);
      // No further script execution is needed, the user will be prompted with the question
      return;
    }
  }
  // On est dans une deuxième exécution, il faut récupérer le texte de sortie
  // de la première exécution
  let first_text=""
  try{
    first_text=fs.readFileSync('_temp_content_import_','utf-8')
    fs.rmSync('_temp_content_import_')
  }catch(err){
    if (err && err.code !== 'ENOENT'){throw err}
  }
  if (answers){importPdfs()};

  ContentScript.done(first_text+"\r\n"+textToInsert.join("\r\n"))

})();