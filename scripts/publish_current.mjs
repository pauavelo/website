import { ContentScript } from '@frontmatter/extensibility';
import { spawn } from 'node:child_process';
import fs from "fs";
import path from 'path';

const { command, scriptPath, workspacePath, filePath, frontMatter, answers } = ContentScript.getArguments();

var dir = path.dirname(filePath)
var msg = path.relative(path.join(workspacePath,'content'),dir);
var fileDate = new Date (frontMatter.date);

if (!fileDate.valueOf()){throw new Error("La date de l'article n'est pas valide ! Corrigez la avant de publier.");}

// S'il s'agit d'une balade, il faut mettre aussi à jour le fichier d'index
let dirParent = path.resolve(dir,"..")
if (path.basename(dirParent).toLowerCase() == "balades"){
  let indexFile = path.join(dirParent,'_index.md')
  let year = new Date(frontMatter.date).getFullYear();
  if (fs.existsSync(indexFile)) {
    let data = fs.readFileSync(indexFile, 'utf8');
    let indexDate = new Date(data.match(/^date: *(.*)$/m)[1]);
    if (!indexDate.valueOf() || // La date du fichier d'index n'est pas valide
        (indexDate < fileDate) || // La date du fichier d'index est antérieure
        (indexDate.getMonth() == fileDate.getMonth() && indexDate != fileDate)
        /*  La date du fichier d'index porte sur le même mois, et elle est différente
         * on suppose qu'on a corrigé la date de la dernière balade, mais cela peut
         * engendrer une petite incohérence si on a deux balades dans le même mois et que l'on 
         * publie la plus ancienne en dernier. La date de l'index sera alors la date
         * de la plus ancienne balade.*/
      ){
      // on met à jour la date du fichier d'index
      fs.writeFileSync(indexFile,data.replace(/^date: .*$/gm, `date: ${frontMatter.date}`));
      await execGitCmd(["add",indexFile])
    }
  }
  else{
    // Si on n'a pas encore de fichier d'index, il faut en créer un.
    fs.writeFileSync(indexFile,
`---
title: Balades ${year}
date: ${frontMatter.date}
author: Pau à Vélo
tags:
  - balade
---
Retrouvez sur cette page toutes les photos et les plans des balades faites en ${year} par notre association.
`   )
    await execGitCmd(["add",indexFile])
  }
}


execGitCmd(["add",dir+"/*"])
.then(() => execGitCmd(["commit","-m",msg]))//.catch( (e) => console.log({e}))
.then(() => execGitCmd(["push","origin","master"]))
.then(() => setTimeout(() => ContentScript.open("http://build.pauavelo.fr"), 3000));

/*
* Fonction récupérée de https://github.com/pankajladhar/run-git-command
* Installé initialement sous forme de module, mais marchait mal, alors
* J'ai copié collé et simplifié le code
*/
function execGitCmd(args) {
  return new Promise(function (resolve, reject) {
      if (!args.length) {
          reject("No arguments were given");
      }
      var commandExecuter = spawn('git', args);
      var stdOutData = '';
      var stderrData = '';
      commandExecuter.stdout.on('data', function (data) { return stdOutData += data; });
      commandExecuter.stderr.on('data', function (data) { return stderrData += data; });
      commandExecuter.on('close', function (code) { return code != 0 ? reject(stdOutData.toString()) : resolve(stdOutData.toString()); });
  });
}
